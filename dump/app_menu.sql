create table menu
(
    id   bigint unsigned auto_increment
        primary key,
    path varchar(255) not null,
    name varchar(255) not null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO app.menu (id, path, name) VALUES (3, '/', 'Home');
INSERT INTO app.menu (id, path, name) VALUES (4, '/account', 'Account');
INSERT INTO app.menu (id, path, name) VALUES (5, '/cart', 'Cart');
INSERT INTO app.menu (id, path, name) VALUES (6, '/products', 'Our services');
INSERT INTO app.menu (id, path, name) VALUES (7, '/privacy-policy', 'Privacy Policy');
INSERT INTO app.menu (id, path, name) VALUES (8, '/cart/checkout', 'Checkout');
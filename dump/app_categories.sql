create table categories
(
    id       bigint unsigned auto_increment
        primary key,
    category text not null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO app.categories (id, category) VALUES (2, 'SEO');
INSERT INTO app.categories (id, category) VALUES (3, '123123');
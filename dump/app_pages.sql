create table pages
(
    id         bigint unsigned auto_increment
        primary key,
    path       varchar(255) not null,
    title      varchar(255) not null,
    content    text         not null,
    created_at timestamp    null,
    updated_at timestamp    null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO app.pages (id, path, title, content, created_at, updated_at) VALUES (5, 'home', 'We welcome you', '<p>Today we are a successful company providing professional services for creating websites and their support, for promoting websites in search engines and their support, for advertising on the Internet.<br>Addressing to us, each our client receives complex service of any needs in sphere of advertising on the Internet, after all experts work with you!</p>', '2020-07-16 19:00:36', '2020-07-16 19:00:36');
create table users
(
    id                bigint unsigned auto_increment
        primary key,
    name              varchar(255) not null,
    email             varchar(255) not null,
    email_verified_at timestamp    null,
    password          varchar(255) not null,
    remember_token    varchar(100) null,
    created_at        timestamp    null,
    updated_at        timestamp    null,
    constraint users_email_unique
        unique (email)
)
    collate = utf8mb4_unicode_ci;

INSERT INTO app.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, admin, routing_number, account_number, account_holder_name, account_holder_type, ba_token, cus_token, valet_verified) VALUES (1, 'admin', 'admin@app.test', null, '$2y$10$fH.zIYHh0bzOriin65eEKO3T6SexcxC3dEtamVKOIBJK3MG6rb8du', null, '2020-06-25 18:14:43', '2020-08-13 22:01:51', 1, '110000000', '000123456789', 'John Smith', 'individual', 'ba_1HFoUUBKhkUR1trg4OOpR4zh', 'cus_HpTREnpH0Qkyj5', 1);
INSERT INTO app.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, admin, routing_number, account_number, account_holder_name, account_holder_type, ba_token, cus_token, valet_verified) VALUES (2, 'Lasley Nilsen', 'lasley@app.test', null, '$2y$10$Q74ZeOaH88ao5kdXb2q7Ru1mP8tVWv.xbAlRt0mjKVzbWDL0MvFT2', null, '2020-08-12 20:28:09', '2020-08-12 20:28:09', 0, '', '', '', '', '', '', 0);
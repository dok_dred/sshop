create table migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO app.migrations (id, migration, batch) VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO app.migrations (id, migration, batch) VALUES (2, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO app.migrations (id, migration, batch) VALUES (4, '2020_06_15_184226_create_pages_table', 2);
INSERT INTO app.migrations (id, migration, batch) VALUES (7, '2020_06_17_180252_create_categories_table', 4);
INSERT INTO app.migrations (id, migration, batch) VALUES (9, '2014_10_12_100000_create_password_resets_table', 6);
INSERT INTO app.migrations (id, migration, batch) VALUES (10, '2020_06_16_182432_create_products_table', 7);
INSERT INTO app.migrations (id, migration, batch) VALUES (12, '2020_07_07_182644_create_menu_table', 8);
INSERT INTO app.migrations (id, migration, batch) VALUES (13, '2020_08_12_175227_take_admin_field', 9);
INSERT INTO app.migrations (id, migration, batch) VALUES (14, '2020_08_12_183712_add_user_data', 10);
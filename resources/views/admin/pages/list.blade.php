@extends('admin.template')
@section('content')
    <div class="mt-5 container">
        {!! Html::link(route('admin.pages.create'),'Новая страница', ['class' => 'btn btn-success']) !!}
        @if($pages)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>№ п/п</th>
                        <th>Наименование</th>
                        <th>Путь</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $page)
                        @if(isset($page))
                            <tr>

                                <td>{{ $page->id }}</td>
                                <td>{!! Html::link(route('admin.pages.edit',['page'=>$page->id]),$page->title,['alt'=>$page->title]) !!}</td>
                                <td>{{ $page->path }}</td>
                                <td>
                                    {!! Form::open(['url'=>route('admin.pages.delete',['page' => $page->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
                                    {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit' ]) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

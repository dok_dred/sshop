@extends('admin.template')
@section('content')
<div class="wrapper container">

    {!! Form::open(['url' => route('admin.pages.store'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}


    <div class="form-group">

        {!! Form::label('path','Путь',['class' => 'col-xs-2 control-label'])   !!}
        <div class="col-xs-8">
            {!! Form::text('path',old('path'),['class' => 'form-control','placeholder'=>'URL страницы'])!!}
        </div>

    </div>


    <div class="form-group">
        {!! Form::label('title', 'Название страницы:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('title', old('title'), ['class' => 'form-control','placeholder'=>'Название страницы']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('content', 'Текст:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::textarea('content', old('content'), ['id'=>'editor','class' => 'form-control','placeholder'=>'Контент страницы']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>



    {!! Form::close() !!}

    <script>
        ClassicEditor
            .create(document.querySelector('#editor'), {
                ckfinder: {
                    uploadUrl: '{{route('admin.upload')}}',
                }
            })
            .catch(error => {
                console.error(error);
            });
    </script>
</div>
@endsection

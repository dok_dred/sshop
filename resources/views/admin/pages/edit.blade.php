@extends('admin.template')
@section('content')
<div class="wrapper container">

    {!! Form::open(['url' => route('admin.pages.update', $page->id),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}


    <div class="form-group">
        {!! Form::hidden('id', $page->id) !!}
        {!! Form::label('title','Название страницы',['class' => 'col-xs-2 control-label'])   !!}
        <div class="col-xs-8">
            {!! Form::text('title', $page->title,['class' => 'form-control', 'placeholder'=>'Введите название страницы'])!!}
        </div>

    </div>

    <div class="form-group">
        {!! Form::label('path','Путь страницы',['class' => 'col-xs-2 control-label'])   !!}
        <div class="col-xs-8">
            {!! Form::text('path', $page->path,['class' => 'form-control', 'placeholder'=>'Введите название страницы'])!!}
        </div>

    </div>

    <div class="form-group">
        {!! Form::label('content', 'Контент:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::textarea('content', $page->content, ['id'=>'editor','class' => 'form-control', 'placeholder'=>'Контент страницы']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>


    {!! Form::close() !!}

    <script>
        ClassicEditor
            .create(document.querySelector('#editor'), {
                ckfinder: {
                    uploadUrl: '{{route('admin.upload')}}',
                }
            })
            .catch(error => {
                console.error(error);
            });
    </script>
</div>
@endsection

@extends('admin.template')
@section('content')
    <div class="mt-5 container">
        {!! Html::link(route('admin.promocodes.create'),'Новый промокод', ['class' => 'btn btn-success']) !!}
        @if($promoCodes)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>№ п/п</th>
                        <th>Промокод</th>
                        <th>Скидка</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($promoCodes as $code)
                        @if(isset($code))
                            <tr>

                                <td>{{ $code->id }}</td>
                                <td>{!! Html::link(route('admin.promocodes.edit',$code->id),$code->code) !!}</td>
                                <td>{{ $code->discount }}</td>
                                <td>
                                    {!! Form::open(['url'=>route('admin.promocodes.delete', $code->id), 'class'=>'form-horizontal','method' => 'POST']) !!}
                                    {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit']) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

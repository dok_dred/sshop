@extends('admin.template')
@section('content')
    <div class="wrapper container">

        {!! Form::open(['url' => route('admin.promocodes.update', $promoCode->id),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}


        <div class="form-group">

            {!! Form::hidden('id', $promoCode->id) !!}
            {!! Form::label('code', 'Код промокода', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('code', $promoCode->code, ['class' => 'form-control','placeholder'=>'Введите код промокода'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('discount', 'Скидка в %', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('discount', $promoCode->discount, ['class' => 'form-control','placeholder'=>'Введите скидку в %'])!!}
            </div>

        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>


        {!! Form::close() !!}
    </div>
@endsection

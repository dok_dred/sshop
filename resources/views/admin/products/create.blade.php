@extends('admin.template')
@section('content')
    <div class="wrapper container">

        {!! Form::open(['url' => route('admin.products.store'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}
        <div class="form-group">
            {!! Form::label('title', 'Название продукта:',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('title', old('title'), ['class' => 'form-control','placeholder'=>'Название продукта']) !!}
            </div>
        </div>

        <div class="form-group">

            {!! Form::label('price','Цена',['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('price', old('price') ,['class' => 'form-control','placeholder'=>'Цена'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('discount', 'Cкидка %', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('discount', old('discount') ,['class' => 'form-control','placeholder'=>'Cкидка %'])!!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('photo', 'Изображение:',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::file('photo[]', ['class' => 'filestyle', 'data-buttonText'=>'Выберите изображение','data-buttonName'=>'btn-primary','data-placeholder'=>'Файла нет', 'accept'=>'image/*', 'multiple']) !!}
            </div>
        </div>

        <div class="form-group">

            {!! Form::label('category','Категория',['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::select('category', $categories,['class' => 'form-control','placeholder'=>'Выберите категорию'])!!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('description', 'Описание товара:',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::textarea('description', old('description'), ['id'=>'editor','class' => 'form-control','placeholder'=>'Описание товара']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>


        {!! Form::close() !!}

        <script>
            ClassicEditor
                .create(document.querySelector('#editor'), {
                    ckfinder: {
                        uploadUrl: '{{route('admin.upload')}}',
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        </script>
    </div>
@endsection

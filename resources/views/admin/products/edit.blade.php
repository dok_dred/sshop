@extends('admin.template')
@section('content')
    <div class="wrapper container">

        {!! Form::open(['url' => route('admin.products.update',$product->id),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}
        <div class="form-group">
            {!! Form::label('title', 'Название продукта:',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('title', $product->title, ['class' => 'form-control','placeholder'=>'Название продукта']) !!}
            </div>
        </div>

        <div class="form-group">

            {!! Form::label('price','Цена',['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('price', $product->price ,['class' => 'form-control','placeholder'=>'Цена'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('discount','Cкидка %',['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('discount', $product->discount ,['class' => 'form-control','placeholder'=>'Cкидка %'])!!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('old_image', 'Текущие изображения:',['class'=>'col-xs-2 control-label']) !!}
            <div class="alert alert-info">Если вам необходимо обновить изображение, то его нужно загрузить заново, с помощью формы ниже.</div>
            <div class="col-xs-8">
                @foreach($product->photo as $photo)
                    {!! Html::image($photo, '', ['class' => 'img-center']) !!}
                @endforeach
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('photo', 'Новые изображения:',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::file('photo[]', ['class' => 'filestyle', 'data-buttonText'=>'Выберите изображения','data-buttonName'=>'btn-primary','data-placeholder'=>'Файла нет', 'accept'=>'image/*', 'multiple'] ) !!}
            </div>
        </div>

        <div class="form-group">

            {!! Form::label('category','Категория',['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::select('category', $categories, $product->category->id,['class' => 'form-control','placeholder'=>'Выберите категорию'])!!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('description', 'Описание товара:',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::textarea('description', $product->description, ['id'=>'editor','class' => 'form-control','placeholder'=>'Описание товара']) !!}
            </div>
        </div>

        @if($product->reviews !== [])
        <div class="form-group">
            <h2>Комментарии к продукту:</h2>
            @foreach($product->reviews as $review)
                <hr>
                    <div class="col-xs-8">
                        <strong>User: <i>{{$review['name']}}</i></strong>
                        <p>{{$review['comment']}}</p>
                        <a class="btn btn-danger" href="{{route('admin.products.comment.delete', ['product'=>$product->id, 'comment'=> $review['id']])}}">Удалить</a>
                    </div>
            @endforeach
        </div>
        @endif

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>


        {!! Form::close() !!}

        <script>
            ClassicEditor
                .create(document.querySelector('#editor'), {
                    ckfinder: {
                        uploadUrl: '{{route('admin.upload')}}',
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        </script>
    </div>
@endsection

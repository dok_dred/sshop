@extends('admin.template')
@section('content')
    <div class="mt-5 container">
        {!! Html::link(route('admin.products.create'),'Новый продукт', ['class' => 'btn btn-success']) !!}
        @if($products)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>№ п/п</th>
                        <th>Наименование</th>
                        <th>Категория</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        @if(isset($product))
                            <tr>

                                <td>{{ $product->id }}</td>
                                <td>{!! Html::link(route('admin.products.edit',['product'=>$product->id]),$product->title,['alt'=>$product->title]) !!}</td>
                                <td>{{ $product->category->category }}</td>
                                <td>
                                    {!! Form::open(['url'=>route('admin.products.delete',['product' => $product->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
                                    {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit' ]) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

@extends('admin.template')
@section('content')
    <div class="wrapper container">

        {!! Form::open(['url' => route('admin.menu.store'), 'class'=>'form-horizontal', 'method'=>'POST', 'enctype'=>'multipart/form-data']) !!}

        <div class="form-group">

            {!! Form::label('name', 'Название пункта меню', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('name', old('name'), ['class' => 'form-control','placeholder'=>'Введите название пункта меню'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('path', 'Путь к странице', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('path', old('path'), ['class' => 'form-control','placeholder'=>'Введите путь к странице'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('parent_id', 'Родительский пункт меню', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::select('parent_id', $menuList, old('parent_id'), ['class' => 'form-control','placeholder'=>'Выберите родительский пункт меню'])!!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('sort','Сортировка',['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::number('sort', old('sort'), ['class' => 'form-control'])!!}
            </div>

        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>

        {!! Form::close() !!}
    </div>
@endsection

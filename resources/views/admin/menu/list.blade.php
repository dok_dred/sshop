@extends('admin.template')
@section('content')
    <div class="mt-5 container">
        {!! Html::link(route('admin.menu.create'),'Новый пункт меню', ['class' => 'btn btn-success']) !!}
        @if($menuList)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>№ п/п</th>
                        <th>Наименование</th>
                        <th>Путь</th>
                        <th>Родительский пункт меню</th>
                        <th>Сортировка</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($menuList as $menu)
                        @if(isset($menu))
                            <tr>

                                <td>{{ $menu->id }}</td>
                                <td>{!! Html::link(route('admin.menu.edit',['menu' => $menu->id]),$menu->name,['alt'=>$menu->name]) !!}</td>
                                <td>{{ $menu->path }}</td>
                                <td>
                                    @if($menu->parent_id !== 0)
                                        {!! Html::link(route('admin.menu.edit',['menu' => $menu->parent_id]),$menuList[$menu->parent_id]->name,['alt'=>$menuList[$menu->parent_id]->name]) !!}
                                    @endif
                                </td>
                                <td>{{ $menu->sort }}</td>
                                <td>
                                    {!! Form::open(['url'=>route('admin.menu.delete',['menu' => $menu->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
                                    {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit' ]) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

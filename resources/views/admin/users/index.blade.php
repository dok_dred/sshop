@extends('admin.template')

@section('content')
    <div class="mt-5 container">
        @if($users)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>Админ</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        @if(isset($user))
                            <tr>

                                <td>{{ $user->email }}</td>
                                <td>{{ $user->isAdmin() ? 'да' : 'нет' }}</td>
                                <td>
                                    @if($user->isAdmin())
                                        <a class="btn btn-success" href="{{route('admin.users.unset', $user->id)}}">Изьять
                                            права администратора</a>
                                    @else
                                        <a class="btn btn-warning" href="{{route('admin.users.set', $user->id)}}">Сделать
                                            администратором</a>
                                    @endif
                                </td>
                                <td><a class="btn btn-danger" href="{{route('admin.users.delete', $user->id)}}">Удалить
                                        пользователя</a></td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

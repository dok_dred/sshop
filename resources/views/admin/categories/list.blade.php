@extends('admin.template')
@section('content')
    <div class="mt-5 container">
        {!! Html::link(route('admin.categories.create'),'Новая категория', ['class' => 'btn btn-success']) !!}
        @if($categories)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>№ п/п</th>
                        <th>Категория</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        @if(isset($category))
                            <tr>

                                <td>{{ $category->id }}</td>
                                <td>{!! Html::link(route('admin.categories.edit',['category'=>$category->id]),$category->category,['alt'=>$category->category]) !!}</td>
                                <td>
                                    {!! Form::open(['url'=>route('admin.categories.delete',['category' => $category->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
                                    {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit' ]) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

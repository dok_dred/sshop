@extends('admin.template')
@section('content')
    <div class="wrapper container">

        {!! Form::open(['url' => route('admin.categories.store'), 'class'=>'form-horizontal', 'method'=>'POST', 'enctype'=>'multipart/form-data']) !!}


        <div class="form-group">

            {!! Form::label('category', 'Название категории', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('category', old('category'), ['class' => 'form-control','placeholder'=>'Введите название категории'])!!}
            </div>

        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>


        {!! Form::close() !!}
    </div>
@endsection

<div class="container">
    <div class="row justify-content-center">
        <div class="section-title">
            <h2>{{$page_name}}</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-3 text-center" style="margin-top: 1em;">
            <a class="btn btn-info" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                Разлогиниться
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        @if(Route::currentRouteName() !== "admin.index")
            <div class="col-lg-3 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.index')}}">Главная</a>
            </div>
        @else
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.categories.list')}}">Категории</a>
            </div>
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.pages.list')}}">Страницы</a>
            </div>
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.products.list')}}">Продукты</a>
            </div>
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.menu.list')}}">Пункты меню</a>
            </div>
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.users.index')}}">Пользователи</a>
            </div>
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.config.list')}}">Конфигурация</a>
            </div>
            <div class="col-lg-2 text-center" style="margin-top: 1em;">
                <a class="btn btn-info" href="{{route('admin.promocodes.list')}}">Промокоды</a>
            </div>
        @endif
    </div>
</div>




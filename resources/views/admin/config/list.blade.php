@extends('admin.template')
@section('content')
    <div class="mt-5 container">
        {!! Html::link(route('admin.config.create'),'Новая конфигурация', ['class' => 'btn btn-success']) !!}
        @if($cfgList)
            <div class="table-responsive text-center">
                <table class="mt-5 table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>№ п/п</th>
                        <th>Наименование</th>
                        <th>Ключ</th>
                        <th>Значение</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cfgList as $cfg)
                        @if(isset($cfg))
                            <tr>

                                <td>{{ $cfg->id }}</td>
                                <td>{!! Html::link(route('admin.config.edit',['cfg' => $cfg->id]),$cfg->name,['alt'=>$cfg->name]) !!}</td>
                                <td>{{ $cfg->key }}</td>
                                <td>{{ $cfg->value }}</td>
                                <td>
                                    {!! Form::open(['url'=>route('admin.config.delete',['cfg' => $cfg->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
                                    {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit' ]) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

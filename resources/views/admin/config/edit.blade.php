@extends('admin.template')
@section('content')
    <div class="wrapper container">

        {!! Form::open(['url' => route('admin.config.update', $cfg->id),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}


        <div class="form-group">

            {!! Form::hidden('id', $cfg->id) !!}
            {!! Form::label('name', 'Название конфигурации', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('name', $cfg->name, ['class' => 'form-control','placeholder'=>'Введите название конфигурации'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('key', 'Ключ конфигурации', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('key', $cfg->key, ['class' => 'form-control','placeholder'=>'Введите ключ конфигурации'])!!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('value', 'Значение конфигурации', ['class' => 'col-xs-2 control-label'])   !!}
            <div class="col-xs-8">
                {!! Form::text('value', $cfg->value, ['class' => 'form-control','placeholder'=>'Введите значение конфигурации'])!!}
            </div>

        </div>


        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>


        {!! Form::close() !!}
    </div>
@endsection

@extends('layouts.public')

@section('content')
    <div class="container margin_top">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="heading_space uppercase">{{$page->title}}
                </h2>
                <section class="bottom_half"> {!! $page->content !!} </section>
            </div>
            @foreach($products as $product)
            <div class="col-md-3 col-sm-6">
                <div class="product_wrap bottom_half">
                    <div class="image">
                        <a class="fancybox" href="{{$product->photo}}">
                            <img src="{{$product->photo}}" alt="Product" class="img-responsive">
                        </a>
                    </div>
                    <div class="product_desc">
                        <p><a href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a>
                        </p>
                        <span class="price">
                  <i class="fa fa-usd">
                  </i>{{$product->price}}
                </span>
                        <a class="fancybox" href="{{route("public.cart.add", $product->id)}}">
                            <i class="fa fa-shopping-bag open">
                            </i>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection

@extends('layouts.public')
@section('content')
    <section id="cart" class="padding_bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div id="slider_product" class="cbp margintop40">
                        <div class="cbp-item">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="{{$product->photo}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="js-pagination-slider">
                        <div class="cbp-pagination-item cbp-pagination-active">
                            <img src="{{$product->photo}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="detail_pro margintop40">
                        <h4 class="bottom30">{{$product->title}}</h4>
                        <section class="bottom30"><p>{!! $product->description !!}</p></section>
                        <ul class="review_list marginbottom15">
                            @php
                                $countComment = $product->countComments();
                                $reviewForm = $countComment === 1 ? 'review' : 'review(s)'
                            @endphp
                            <li><a href="#tab2">{{$countComment}} {{$reviewForm}} </a>
                            </li>

                        </ul>
                        <h2 class="price marginbottom15"><i class="fa fa-usd"></i>{{$product->price}}</h2>

                        <div class="cart-buttons">
                            <a class="uppercase border-radius btn-dark"
                               href="{{route('public.cart.add', $product->id)}}"><i
                                    class="fa fa-shopping-basket"></i> &nbsp; Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bottom_half">
        <div class="container">
            <div class="row">
                <div class="clearfix col-md-12">
                    <div class="shop_tab">
                        <ul class="tabs">
                            <li class="active" rel="tab1">
                                <h4 class="heading uppercase">Description</h4>
                            </li>
                            <li rel="tab2" class="">
                                <h4 class="heading uppercase">Customer Review</h4>
                            </li>
                        </ul>
                        <div class="tab_container">
                            <div id="tab1" class="tab_content" style="display: block;">
                                <section><p>{!! $product->description !!}</p></section>
                            </div>

                            <div id="tab2" class="tab_content" style="display: none;">
                                <ol class="commentlist">
                                    @foreach($product->reviews as $review)
                                        <li>
                                            <div class="avator clearfix"><img src="/images/ava.png"
                                                                              class="img-responsive border-radius"
                                                                              alt="author">
                                            </div>
                                            <div class="comment-content">
                                                <strong>User: <i>{{$review['name']}}</i></strong>
                                                <p>{{$review['comment']}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ol>

                                <form method="POST" action="{{route('public.product.comment.add', $product->id)}}"
                                      class="review-form margintop15">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger mt-5">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <label class="control-label">Your Review</label>
                                            <textarea class="form-control" rows="3" name="comment"
                                                      placeholder="Your Review"></textarea>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label for="inputPassword" class="control-label">Name</label>
                                            <input type="text" name="name" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="submit" class="btn-light border-radius" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="feature_product" class="bottom_half">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="heading uppercase bottom30">upsell products</h4>
                </div>
                @foreach($featureProducts as $product)
                    <div class="col-md-3 col-sm-6">
                        <div class="product_wrap bottom_half">
                            <div class="image">
                                <a class="fancybox" href="{{$product->photo}}">
                                    <img src="{{$product->photo}}" alt="Product" class="img-responsive">
                                </a>
                            </div>
                            <div class="product_desc">
                                <p><a href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a>
                                </p>
                                <span class="price">
                  <i class="fa fa-usd">
                  </i>{{$product->price}}
                </span>
                                <a class="fancybox" href="{{route("public.cart.add", $product->id)}}">
                                    <i class="fa fa-shopping-bag open">
                                    </i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@extends('layouts.public')
@section('content')
    <div class="row">
        <div class="col-md-12 text-center">
            <h2 class="heading_space uppercase">{{$page_name}}
            </h2>
        </div>
    </div>
    <div class="row shop-grid list-view">
        <div class="col-md-3 col-sm-6">
            @foreach ($products as $product)
                <div class="product_wrap heading_space">
                    <div class="image">
                        <a class="fancybox" href="{{$product->photo}}"><img src="{{$product->photo}}" alt="Product"
                                                                            class="img-responsive"></a>
                    </div>
                    <div class="product_desc">
                        <p class="title">{{$product->title}} </p>
                        <div class="list_content">
                            <h4 class="bottom30"><a href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a></h4>
                            <section><p>{!! $product->description !!} </p></section>
                            <ul class="review_list bottomtop30">
                                @php
                                    $countComment = $product->countComments();
                                    $reviewForm = $countComment === 1 ? 'review' : 'review(s)'
                                @endphp
                                <li><a href="{{route('public.product.show', $product->id)}}">{{$countComment}} {{$reviewForm}} </a>
                                </li>
                            </ul>
                            <h4 class="price bottom30"><i class="fa fa-usd"></i>{{$product->price}} &nbsp;<span
                                    class="discount"><i
                                        class="fa fa-usd"></i>{{$product->price}}</span></h4>
                            <div class="cart-buttons">
                                <a class="uppercase border-radius btn-dark" href="{{route('public.cart.add', $product->id)}}"><i
                                        class="fa fa-shopping-basket"></i> &nbsp; Add to cart</a>
                            </div>
                        </div>
                        <span class="price"><i class="fa fa-usd"></i>{{$product->price}}</span>
                        <a class="fancybox" href="{{$product->photo}}" data-fancybox-group="gallery"><i
                                class="fa fa-shopping-bag open"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

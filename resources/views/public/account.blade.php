@extends('layouts.public')

@section('content')
    <section id="cart">
        @if (session('success'))
            <div class="alert alert-success mt-5">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger mt-5">
                {{ session('error') }}
            </div>
        @endif
        @guest
            <div class="container">
                <div class="row">
                    <div class="col-md-6 margintop40">
                        <h4 class="heading uppercase bottom30">Create a new account</h4>
                        <form class="contact-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="John Smith">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" id="email"
                                       placeholder="jane.doe@example.com">
                            </div>
                            <div class="form-group">
                                <label for="pass">Password</label>
                                <input type="password" name="password" class="form-control" id="pass"
                                       placeholder="*****">
                            </div>
                            <div class="form-group">
                                <label for="passc">Password confirm</label>
                                <input type="password" name="password_confirmation" class="form-control" id="passc"
                                       placeholder="*****">
                            </div>

                            <input type="submit" class="uppercase margintop40 btn-dark" value="REgister now">
                        </form>

                    </div>
                    <div class="col-md-6 margintop40">
                        <h4 class="heading uppercase bottom30">Login to your account</h4>
                        <form class="contact-form" method="POST" action="{{ route('login')}}">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" id="email-log"
                                       placeholder="jane.doe@example.com">
                            </div>
                            <div class="form-group">
                                <label for="pass">Password</label>
                                <input type="password" name="password" class="form-control" id="pass-log"
                                       placeholder="*****">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="uppercase margintop40 btn-light"
                                       value="Login to your account">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endguest

        @auth
            @php
                $valet = Auth::user()->getValet();
            @endphp
            <div class="mt-5 container">
                @if ($valet->isFilled())
                    <div class="table-responsive text-center">
                        <table class="mt-5 table table-hover table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">Routing numder</th>
                                <th class="text-center">Account number</th>
                                <th class="text-center">Holder name</th>
                                <th class="text-center">Verified</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $valet->getRoutingNumber() }}</td>
                                <td>{{ $valet->getAccountNumber() }}</td>
                                <td>{{ $valet->getAccountHolderName() }}</td>
                                <td>{{ $valet->isVerified() ? 'Yes' : 'No' }}</td>
                                <td><a href="{{route('public.account.clear')}}">Clear</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    @if(!$valet->isVerified())
                        <div class="row">
                            @if(!$valet->isApproved())
                                <div class="col-lg-2 text-center" style="margin-top: 1em;">
                                    <a class="btn btn-info" id="approve" style="display: none"
                                       href="{{ route('public.account.approve') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('token-form').submit();">
                                        Approve account
                                    </a>

                                    <a class="btn btn-info" id="tokenizer"
                                       onclick="event.preventDefault();
                                           var stripe = Stripe('{{$stripePk}}');
                                           token = '';
                                           bankAccount = '';
                                           stripe
                                           .createToken('bank_account', {
                                           country: 'US',
                                           currency: 'usd',
                                           routing_number: '{{ $valet->getRoutingNumber() }}',
                                           account_number: '{{$valet->getAccountNumber()}}',
                                           account_holder_name: '{{ $valet->getAccountHolderName() }}',
                                           account_holder_type: '{{ $valet->getAccountHolderType()}}',
                                           })
                                           .then(function(result) {
                                           bankAccount = result.token.bank_account.id;
                                           token = result.token.id;
                                           // Handle result.error or result.token
                                           console.log(result.token.bank_account.id);

                                           document.getElementById('tokenizer').style.display = 'none';
                                           document.getElementById('approve').style.display = '';
                                           document.getElementById('ba_token').value = token;
                                           document.getElementById('bank_account').value = bankAccount;
                                           console.log(document.getElementById('ba_token').value);
                                           });

                                           ">
                                        Set token
                                    </a>

                                    <form id="token-form" action="{{ route('public.account.approve') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                        <input type="hidden" id="ba_token" name="token" value="">
                                        <input type="hidden" id="bank_account" name="bank_account" value="">
                                    </form>
                                </div>
                            @else
                                <div class="row col-md-6 margintop40">
                                    <h4 class="heading uppercase bottom30">Vefiry account by small deposits</h4>
                                    <p>Check your account and enter deposits from our shop</p>
                                    <form class="contact-form" method="POST"
                                          action="{{ route('public.account.verify') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="firstsecond">First deposit</label>
                                            <input type="text" name="first" class="form-control" id="firstsecond"
                                                   placeholder="00">
                                        </div>
                                        <div class="form-group">
                                            <label for="second">Second deposit</label>
                                            <input type="text" name="second" class="form-control" id="second"
                                                   placeholder="11">
                                        </div>
                                        <input type="submit" class="uppercase margintop40 btn-dark" value="Verify">
                                    </form>
                                </div>
                            @endif
                        </div>
                    @endif
                @else
                    <div class="row col-md-6 margintop40">
                        <h4 class="heading uppercase bottom30">Add new bank</h4>
                        <form class="contact-form" method="POST" action="{{ route('public.account.add') }}">
                            @csrf
                            <div class="form-group">
                                <label for="routing_number">Routing numder</label>
                                <input type="text" name="routing_number" class="form-control" id="routing_number"
                                       placeholder="110000000">
                            </div>
                            <div class="form-group">
                                <label for="account_number">Account number</label>
                                <input type="text" name="account_number" class="form-control" id="account_number"
                                       placeholder="000123456789">
                            </div>
                            <div class="form-group">
                                <label for="account_holder_name">Holder name</label>
                                <input type="text" name="account_holder_name" class="form-control"
                                       id="account_holder_name"
                                       placeholder="John Smith">
                            </div>

                            <input type="submit" class="uppercase margintop40 btn-dark" value="Add">
                        </form>
                    </div>
                @endif
            </div>
        @endauth
    </section>
    <script src="https://js.stripe.com/v3/"></script>
@endsection

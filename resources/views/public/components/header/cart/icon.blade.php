<div class="attr-nav">
    <ul>
        <li class="cart-toggler">
            <a href="#.">
                <i class="fa fa-shopping-cart"></i>
                <span class="badge">{{$cart['qty']}}</span>
            </a>
        </li>
    </ul>
</div>

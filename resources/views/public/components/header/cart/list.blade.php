<ul class="cart-list">
    @foreach($cart['items'] as $item)
        <li>
            @php
                $product = $cart['products'][$item['id']];
            @endphp
            <a href="{{route('public.product.show', $product->id)}}" class="photo">
                <img src="{{$product->photo}}" class="cart-thumb" alt="">
            </a>
            <h6>
                <a href="{{route('public.product.show', $product->id)}}">{{$product->title}}
                </a>
            </h6>
            <p>Qty: {{$item['qty']}}
                <span class="price">${{$product->price}}
                </span>
            </p>
        </li>
    @endforeach
        <li class="total clearfix">
            <div class="pull-left">
                <strong>Total
                </strong>: ${{$cart['total']}}
            </div>
        </li>
        <li class="cart-btn">
            <a href="{{route('public.cart.index')}}" class="active">VIEW CART
            </a>
            <a href="{{route('public.cart.checkout')}}">CHECKOUT
            </a>
        </li>
</ul>

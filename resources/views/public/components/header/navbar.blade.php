<nav class="navbar navbar-default navbar-sticky bootsnav on no-full">
    <div class="container">
        @include('public.components.header.cart.icon')
    <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars">
                </i>
            </button>
            <a class="navbar-brand" href="{{route('public.home')}}">
                <h2>{{config('app.name', "Laravel")}}</h2>
            </a>
        </div>
        <!-- End Header Navigation -->
        @include('public.components.header.menu')
    <!-- /.navbar-collapse -->
        @include('public.components.header.cart.list')
    </div>
</nav>

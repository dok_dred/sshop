<div class="collapse navbar-collapse" id="navbar-menu">
    <ul class="nav navbar-nav navbar-right" data-in="fadeIn" data-out="fadeOut">
        @foreach($menu as $link)
            <li>
                <a href="{{$link->path}}">{{$link->name}}
                </a>
            </li>
        @endforeach
        @auth
            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        @endauth
    </ul>
</div>

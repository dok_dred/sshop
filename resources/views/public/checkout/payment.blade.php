@extends('layouts.public')
@section('content')
    <section id="cart" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2 class="heading_space uppercase">{{$status}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        console.log({{$response}})
    </script>
@endsection

@extends('layouts.public')
@section('content')
    <section id="cart" class="padding">
        <div class="container">
            @if($cart['qty'] !== 0)
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading uppercase marginbottom15">Shopping cart</h4>
                        <div class="table-responsive">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="uppercase">Product photo</th>
                                    <th class="uppercase">Product name</th>
                                    <th class="uppercase">Description</th>
                                    <th class="uppercase">Price</th>
                                    <th class="uppercase">Quantity</th>
                                    <th class="uppercase">Total Price</th>
                                    <th class="uppercase"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart['items'] as $item)
                                    <tr>
                                        @php
                                            $product = $cart['products'][$item['id']];
                                        @endphp
                                        <td class="col-xs-2">
                                            <img class="shopping-product" src="{{$product->photo}}" alt="your product">
                                        </td>
                                        <td class="product-name col-xs-2">
                                            <h5>
                                                <a href="{{route('public.product.show', $product->id)}}">{{$product->title}}
                                                </a></h5>
                                        </td>
                                        <td>
                                            <section>{!! $product->description !!}</section>
                                        </td>
                                        <td class="price">
                                            <h5>${{$product->price}}</h5>
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                <a href="{{route('public.cart.remove.item', $item['__raw_id'])}}">
                                                    - </a>
                                                <span>{{$item['qty']}}</span>
                                                <a href="{{route('public.cart.add', $product->id)}}"> + </a>
                                            </div>
                                        </td>
                                        <td class="price">
                                            <h5>${{$item['total']}}</h5>
                                        </td>
                                        <td class="text-center"><a class="btn-close"
                                                                   href="{{route('public.cart.delete', $item['__raw_id'])}}"><i
                                                    class="fa fa-close"></i></a></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('public.product.list')}}"
                                   class="uppercase btn-light border-radius margintop30">CONTINUE SHOPPING</a>
                            </div>
                            <div class="col-sm-3 text-right">
                                <a href="{{route('public.cart.remove.all')}}"
                                   class="uppercase btn-dark border-radius margintop30">CLEAR SHOPPING CART</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-8 col-xs-4">
                        <div class="shop_measures margintop40">
                            <h4 class="heading uppercase bottom30">cart totals</h4>
                            <table class="table table-responsive">
                                <tbody>
                                <tr>
                                    <td>Cart Totals</td>
                                    <td class="text-right"><h5 class="price">${{$cart['total']}}</h5></td>
                                </tr>
                                </tbody>
                            </table>
                            <a href="{{route('public.cart.checkout')}}"
                               class="uppercase btn-light border-radius margintop30">Proceed to checkout</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="heading_space uppercase">Shopping cart is empty</h2>
                        <div class="col-sm-6 col-sm-offset-3">
                            <a href="{{route('public.product.list')}}"
                               class="uppercase btn-light border-radius margintop30">CONTINUE SHOPPING</a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection

@extends('layouts.public')
@section('content')
    <section id="cart" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="col-sm-6 col-sm-offset-3">
                        @if($sessionId !== '')
                            <div class="row">
                                <button class="uppercase btn-dark border-radius margintop30" id="checkout-button">Pay
                                    via
                                    card
                                </button>
                            </div>
                            @auth
                                @if(Auth::user()->getValet()->isVerified())
                                    <a href="{{route('public.payment.bank')}}"
                                       class="uppercase btn-light border-radius margintop30">pay via bank</a>
                                @else

                                    <div class="row"><a href="{{route('public.account')}}"
                                                        class="uppercase btn-light border-radius margintop30">Verify
                                            bank</a>
                                    </div>



                                @endif
                            @endauth
                            @guest
                                <div class="row"><a href="{{route('public.account')}}"
                                                    class="uppercase btn-light border-radius margintop30">Register to
                                        pay via bank</a>
                                </div>
                            @endguest
                            <div class="row margintop30">or</div>
                        @endif
                        <div class="row"><a href="{{route('public.product.list')}}"
                                            class="uppercase btn-light border-radius margintop30">CONTINUE SHOPPING</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var stripe = Stripe('{{$stripePk}}');
        var checkoutButton = document.getElementById('checkout-button');

        checkoutButton.addEventListener('click', function () {
            stripe.redirectToCheckout({
                sessionId: '{{$sessionId}}'
            }).then(function (result) {
                console.log(result)
                // If `redirectToCheckout` fails due to a browser or network
                // error, display the localized error message to your customer
                // using `result.error.message`.
            });
        });
    </script>
@endsection

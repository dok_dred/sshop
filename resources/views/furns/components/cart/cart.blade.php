<!-- OffCanvas Cart Start -->
<div id="offcanvas-cart" class="offcanvas offcanvas-cart">
    <div class="inner">
        <div class="head">
            <span class="title">Cart</span>
            <button class="offcanvas-close">×</button>
        </div>
        <div class="body customScroll">
            <ul class="minicart-product-list">
                @foreach($cart['items'] as $item)
                    @php
                        $product = $cart['products'][$item['id']];
                    @endphp
                    <li>
                        <a href="{{route('public.product.show', $product->id)}}" class="image"><img
                                src="{{ $product->photo[0]}}"
                                alt="{{ $product->title }}"></a>
                        <div class="content">
                            <a href="{{route('public.product.show', $product->id)}}"
                               class="title">{{ $product->title }}</a>
                            <span class="quantity-price">{{$item['qty']}} x <span
                                    class="amount">${{sprintf("%.2f",$product->priceWithAllDiscount())}}</span></span>
                            <a href="{{route('public.cart.delete', $product->id)}}" class="remove">×</a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="foot">
            <div class="sub-total">
                <table class="table">
                    <tbody>
                    <tr>
                        <td class="text-left">Total :</td>
                        <td class="text-right theme-color">${{sprintf("%.2f", $cart['total'])}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <a href="{{route('public.cart.index')}}" class="btn btn-dark btn-hover-primary mb-6">view cart</a>
                <a href="{{route('public.cart.checkout')}}" class="btn btn-outline-dark current-btn">checkout</a>
            </div>
        </div>
    </div>
</div>
<!-- OffCanvas Cart End -->

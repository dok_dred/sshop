<div class="footer-area">
    <div class="footer-container">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <!-- Start single blog -->
                    <div class="col-md-6 col-lg-4 mb-md-30px mb-lm-30px" data-aos="fade-up" data-aos-delay="200">
                        <div class="single-wedge">
                            <h4 class="footer-herading">about us</h4>
                            <p class="about-text">{{\App\Model\Config::getValue('about')}}</p>
                        </div>
                    </div>
                    <!-- End single blog -->
                    @include('furns.components.menu.footer_menu')
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row flex-sm-row-reverse">
                    <div class="col-md-6 text-right">
                        <div class="payment-link">
                            <img src="{{ asset("assets/furns/images/icons/payment.png") }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 text-left">
                        <p class="copy-text">Copyright © {{date("Y")}} <a class="company-name"
                                                                          href="{{route("public.home")}}">
                                {{config('app.name', "Laravel")}}</a>. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php

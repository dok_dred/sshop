@foreach($items as $item)
    @if(count($item->children) !== 0)
        <li><a href="#"><span class="menu-text">{{$item->name}}</span></a>
            <ul class="sub-menu">
                @php
                    $children = $item->children;
                    $item->children = [];
                    $children[] = $item;
                @endphp
                @include('furns.components.menu.mobile_children', ['items' => $children])
            </ul>
        </li>
    @else
        <li><span class="menu-text"><a href="{{$item->path}}">{{$item->name}}</a></span></li>
    @endif
@endforeach

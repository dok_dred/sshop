@for($k = 0; $k < count($footerMenu); $k ++)
    @if($k === 0)
        <!-- Start single blog -->
        <div class="col-md-6 col-sm-6 col-lg-3 mb-md-30px mb-lm-30px" data-aos="fade-up" data-aos-delay="400">
            <div class="single-wedge">
                <div class="footer-links">
                    <div class="footer-row">
                        <ul class="align-items-center">
    @endif
                            <li class="li"><a class="single-link" href="{{$footerMenu[$k]->path}}">{{$footerMenu[$k]->name}}</a></li>
    @if(($k + 1) % 6 === 0 && $k > 0)
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End single blog -->
        <!-- Start single blog -->
        <div class="col-md-6 col-sm-6 col-lg-3 mb-md-30px mb-lm-30px" data-aos="fade-up" data-aos-delay="400">
            <div class="single-wedge">
                <div class="footer-links">
                    <div class="footer-row">
                        <ul class="align-items-center">
    @endif
    @if($k === count($footerMenu) - 1)
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start single blog -->
    @endif
@endfor

<!-- OffCanvas Menu Start -->
<div id="offcanvas-mobile-menu" class="offcanvas offcanvas-mobile-menu">
    <button class="offcanvas-close"></button>
    <div class="inner customScroll">
        <div class="offcanvas-menu mb-4">
            <ul>@include('furns.components.menu.mobile_children', ['items' => $menu])</ul>
        </div>
    </div>
</div>
<!-- OffCanvas Menu End -->

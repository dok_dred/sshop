<div class="main-menu">
    <ul>
        @foreach($menu as $item)
            @if(count($item->children) !== 0)
                <li class="dropdown"><a href="{{$item->path}}">{{$item->name}} <i class="ion-ios-arrow-down"></i></a>
                    <ul class="sub-menu">
                        @foreach($item->children as $subItem)
                            @if(count($subItem->children) !== 0)
                                <li class="dropdown position-static">
                                    <a href="{{$subItem->path}}">{{$subItem->name}} <i class="ion-ios-arrow-right"></i></a>
                                    @include("furns.components.menu.desktop_children", ['items' => $subItem->children])
                                </li>
                            @else
                                <li class="dropdown"><a href="{{$subItem->path}}">{{$subItem->name}} </a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            @else
                <li><a href="{{$item->path}}">{{$item->name}}</a></li>
            @endif
        @endforeach
    </ul>
</div>

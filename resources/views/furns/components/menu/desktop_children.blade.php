<ul class="sub-menu sub-menu-2">
    @foreach($items as $item)
        @if(count($item->children) !== 0)
            <li><a href="{{$item->path}}">{{$item->name}}</a></li>
            @include('furns.components.menu.desktop_children', ['items' => $item->children])
        @else
            <li><a href="{{$item->path}}">{{$item->name}}</a></li>
        @endif
    @endforeach
</ul>


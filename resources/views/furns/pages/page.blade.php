@extends('layouts.furns')
@section('content')
    <!--Privacy Policy area start-->
    <div class="privacy_policy_main_area pb-100px pt-100px">
        <div class="container">
            <div class="container-inner">
                <div class="row">
                    <div class="col-12">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

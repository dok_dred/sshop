@extends('layouts.furns')

@section('content')

    <!-- Hero/Intro Slider Start -->
    <div class="section ">
        <div class="hero-slider swiper-container slider-nav-style-1 slider-dot-style-1">
            <!-- Hero slider Active -->
            <div class="swiper-wrapper">
                <!-- Single slider item -->
                @foreach($slider as $product)
                    <div class="hero-slide-item slider-height swiper-slide d-flex">
                        <div class="container align-self-center">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-7 col-sm-7 align-self-center">
                                    <div class="hero-slide-content slider-animated-1">
                                        <span class="category">New Products</span>
                                        <h2 class="title-1">{{$product->title}} </h2>
                                        <p>{!! sprintf('%s...', substr($product->description, 0, 150)) !!}</p>
                                        <a href="{{route('public.product.show', $product->id)}}"
                                           class="btn btn-lg btn-primary btn-hover-dark mt-5">Shop Now</a>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-5 col-md-5 col-sm-5">
                                    <div class="hero-slide-image">
                                        <img src="{{$product->photo[0]}}" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
            <!-- Add Arrows -->
            <div class="swiper-buttons">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>

    <!-- Hero/Intro Slider End -->

    <!-- Product Category Start -->
    <div class="section pt-8">
        {{--        <div class="container">
                    <div class="category-slider swiper-container" data-aos="fade-up">
                        <div class="category-wrapper swiper-wrapper">
                            <!-- Single Category -->
                            <div class=" swiper-slide">
                                <a href="shop-left-sidebar.html" class="category-inner ">
                                    <div class="category-single-item">
                                        <img src="{{ asset("assets/furns/images/icons/1.png") }}" alt="">
                                        <span class="title">Office Chair</span>
                                    </div>
                                </a>
                            </div>
                            <!-- Single Category -->
                            <div class=" swiper-slide">
                                <a href="shop-left-sidebar.html" class="category-inner ">
                                    <div class="category-single-item">
                                        <img src="{{ asset("assets/furns/images/icons/2.png") }}" alt="">
                                        <span class="title">Book Shelf</span>
                                    </div>
                                </a>
                            </div>
                            <!-- Single Category -->
                            <div class=" swiper-slide">
                                <a href="shop-left-sidebar.html" class="category-inner ">
                                    <div class="category-single-item">
                                        <img src="{{ asset("assets/furns/images/icons/3.png") }}" alt="">
                                        <span class="title">Light & Sofa</span>
                                    </div>
                                </a>
                            </div>
                            <!-- Single Category -->
                            <div class=" swiper-slide">
                                <a href="shop-left-sidebar.html" class="category-inner ">
                                    <div class="category-single-item">
                                        <img src="{{ asset("assets/furns/images/icons/4.png") }}" alt="">
                                        <span class="title">Reading Table</span>
                                    </div>
                                </a>
                            </div>
                            <!-- Single Category -->
                            <div class="swiper-slide">
                                <a href="shop-left-sidebar.html" class="category-inner ">
                                    <div class="category-single-item">
                                        <img src="{{ asset("assets/furns/images/icons/5.png") }}" alt="">
                                        <span class="title">Corner Table</span>
                                    </div>
                                </a>
                            </div>
                            <!-- Single Category -->
                        </div>
                    </div>
                </div>--}}
    </div>

    <!-- Product Category End -->

    <!-- Product tab Area Start -->
    <div class="section product-tab-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" data-aos="fade-up">
                    <div class="section-title mb-0">
                        <h2 class="title">{{$page->title}}</h2>
                        <p class="sub-title mb-6">{!! $page->content !!}</p>
                    </div>
                </div>

                <div class="col-md-12 text-center" data-aos="fade-up">
                    <div class="section-title mb-0">
                        <br>
                        <br>
                    </div>
                </div>


                <div class="col-md-12 text-center" data-aos="fade-up">
                    <div class="section-title mb-0">
                        <h2 class="title">Our Products</h2>
                    </div>
                </div>

                <!-- Tab Start -->
                <div class="col-md-12 text-center mb-8" data-aos="fade-up" data-aos-delay="200">
                    <ul class="product-tab-nav nav justify-content-center">
                        @foreach($categories as $category)
                            <li class="nav-item"><a class="nav-link {{$category->id === 0 ? 'active': '' }}"
                                                    data-bs-toggle="tab"
                                                    href="#tab-{{$category->id}}">{{$category->category}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <!-- Tab End -->
            </div>
            <div class="row">
                <div class="col">
                    <div class="tab-content">
                        @foreach($products as $category => $productList)
                            <div class="tab-pane fade show {{$category === 0 ? 'active': '' }}" id="tab-{{$category}}">
                                <div class="row">
                                    @foreach($productList as $product)
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-6" data-aos="fade-up"
                                             data-aos-delay="200">
                                            <!-- Single Product -->
                                            <div class="product mb-5">
                                                <div class="thumb">
                                                    <a href="{{route('public.product.show', $product->id)}}"
                                                       class="image">
                                                        <img src="{{$product->photo[0]}}" alt="Product"/>
                                                        <img class="hover-image" src="{{$product->photo[0]}}"
                                                             alt="Product"/>
                                                    </a>
                                                    @if($product->discount > 0)
                                                        <span class="badges">
                                            <span class="sale">{{$product->discount}} %</span>
                                        </span>
                                                    @endif
                                                    <div class="actions">
                                                        <a class="action quickview" data-link-action="quickview"
                                                           title="Quick view" data-bs-toggle="modal"
                                                           data-bs-target="#modal-{{$product->id}}"><i
                                                                class="icon-size-fullscreen"></i></a>
                                                    </div>
                                                    <a href="{{route('public.cart.add', $product->id)}}"
                                                       class="add-to-cart">Add
                                                        To Cart
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="title"><a
                                                            href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a>
                                                    </h5>
                                                    <span class="price">
                                                @if($product->discount > 0)
                                                            <span
                                                                class="new">${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2, PHP_ROUND_HALF_DOWN))}}</span>
                                                            <span
                                                                class="old">${{sprintf("%.2f", $product->price)}}</span>
                                                        @else
                                                            <span
                                                                class="new">${{sprintf("%.2f", $product->price)}}</span>
                                                        @endif
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product tab Area End -->

    @foreach($products[0] as $product)
        <div class="modal fade" id="modal-{{$product->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">x</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5 col-sm-12 col-xs-12 mb-lm-30px mb-sm-30px">
                                <!-- Swiper -->
                                <div class="swiper-container gallery-top gallery-top-{{$product->id}} mb-4">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div
                                    class="swiper-container gallery-thumbs gallery-thumbs-{{$product->id}} slider-nav-style-1 small-nav">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-next swiper-button-next-{{$product->id}}"></div>
                                        <div class="swiper-button-prev swiper-button-prev-{{$product->id}}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div class="product-details-content quickview-content">
                                    <h2>{{$product->title}}</h2>
                                    <p class="reference"></p>
                                    <div class="pro-details-rating-wrap">
                                        @if($product->countComments() > 0)
                                            <span class="read-review"><a class="reviews"
                                                                         href="{{route('public.product.show', $product->id)}}">Read {{$product->countComments() <= 1 ? 'review' : 'review(s)'}} {{$product->countComments()}}</a></span>
                                        @endif
                                    </div>
                                    <div class="pricing-meta">
                                        @if($product->discount > 0)
                                            <span>${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                            <span
                                                style="text-decoration: line-through;color: #999;">${{sprintf("%.2f", $product->price)}}</span>
                                        @else
                                            <span>${{sprintf("%.2f", $product->price)}}</span>
                                        @endif
                                    </div>
                                    <p class="quickview-para">{!! $product->description !!}
                                    </p>
                                    <div class="pro-details-quality">
                                        {!! Form::open(['url' => route('public.cart.add_qty',$product->id),'class'=>'pro-details-quality', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" type="text" name="quantity" value="1"/>
                                        </div>
                                        <div class="pro-details-cart btn-hover">
                                            {!! Form::button('Add
                                                To Cart', ['class' => 'add-cart btn btn-primary btn-hover-primary ml-4','type'=>'submit']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="pro-details-wish-com">
                                    </div>
                                    <div class="pro-details-social-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@extends('layouts.furns')
@section('content')
    <!-- Product Details Area Start -->
    <div class="product-details-area pt-100px pb-100px">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-12 col-xs-12 mb-lm-30px mb-md-30px mb-sm-30px">
                    <!-- Swiper -->
                    <div class="swiper-container zoom-top">
                        <div class="swiper-wrapper">
                            @foreach($product->photo as $photo)
                                <div class="swiper-slide zoom-image-hover">
                                    <img class="img-responsive m-auto" src="{{$photo}}" alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-container zoom-thumbs slider-nav-style-1 small-nav mt-3 mb-3">
                        <div class="swiper-wrapper">
                            @foreach($product->photo as $photo)
                                <div class="swiper-slide">
                                    <img class="img-responsive m-auto" src="{{$photo}}"
                                         alt="">
                                </div>
                            @endforeach
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-buttons">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-delay="200">
                    <div class="product-details-content quickview-content">
                        <h2>{{$product->title}}</h2>
                        <p class="reference"></p>
                        <div class="pro-details-rating-wrap">
                        </div>
                        <div class="pricing-meta">
                            @if($product->discount > 0)
                                <span>${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                <span
                                    style="text-decoration: line-through;color: #999;">${{sprintf("%.2f", $product->price)}}</span>
                            @else
                                <span>${{sprintf("%.2f", $product->price)}}</span>
                            @endif
                        </div>
                        <p class="quickview-para">{!! sprintf('%s...', substr($product->description, 0, 150)) !!}
                        </p>
                        <div class="pro-details-quality">
                            {!! Form::open(['url' => route('public.cart.add_qty',$product->id),'class'=>'pro-details-quality', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                            <div class="cart-plus-minus">
                                <input class="cart-plus-minus-box" type="text" name="quantity" value="1"/>
                            </div>
                            <div class="pro-details-cart btn-hover">
                                {!! Form::button('Add
                                    To Cart', ['class' => 'add-cart btn btn-primary btn-hover-primary ml-4','type'=>'submit']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="pro-details-wish-com">
                        </div>
                        <div class="pro-details-social-info">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product Details Area End -->

    <!-- product details description area start -->
    <div class="description-review-area pb-100px" data-aos="fade-up" data-aos-delay="200">
        <div class="container">
            <div class="description-review-wrapper">
                <div class="description-review-topbar nav">
                    <a class="active" data-bs-toggle="tab" href="#des-details1">Description</a>
                    <a data-bs-toggle="tab" href="#des-details2">Reviews {{$product->countComments() !== 0 ? '('.$product->countComments().')': "" }}</a>
                </div>
                <div class="tab-content description-review-bottom">
                    <div id="des-details1" class="tab-pane active">
                        <div class="product-description-wrapper">
                            {!! $product->description !!}
                        </div>
                    </div>
                    <div id="des-details2" class="tab-pane">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="review-wrapper">
                                    @foreach($product->reviews as $review)
                                        <div class="single-review">
                                            <div class="review-img">
                                                <img src="{{asset('assets/furns/images/review-image/placeholder_face.png')}}" width="120" height="120" alt=""/>
                                            </div>
                                            <div class="review-content">
                                                <div class="review-top-wrap">
                                                    <div class="review-left">
                                                        <div class="review-name">
                                                            <h4>User: {{$review['name']}}</h4>
                                                        </div>
                                                        <div class="rating-product">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="review-bottom">
                                                    <p>
                                                        {{$review['comment']}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="ratting-form-wrapper pl-50">
                                    <h3>Add a Review</h3>
                                    <div class="ratting-form">
                                        <form method="POST"
                                              action="{{route('public.product.comment.add', $product->id)}}">
                                            <div class="star-box">
                                            </div>
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger mt-5">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="row">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <div class="col-md-6">
                                                    <div class="rating-form-style">
                                                        <input name="name" placeholder="Name" type="text"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="rating-form-style form-submit">
                                                        <textarea name="comment" placeholder="Message"></textarea>
                                                        <button class="btn btn-primary btn-hover-color-primary "
                                                                type="submit" value="Submit">Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product details description area end -->

    <!-- New Product Start -->
    <div class="section pb-100px" data-aos="fade-up" data-aos-delay="200">
        <div class="container">
            <!-- section title start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-left mb-11">
                        <h2 class="title">You Might Also Like</h2>
                    </div>
                </div>
            </div>
            <!-- section title start -->
            <div class="new-product-slider swiper-container slider-nav-style-1" data-aos="fade-up" data-aos-delay="200">
                <div class="new-product-wrapper swiper-wrapper">
                    <!-- Single Product -->
                    @foreach($featureProducts as $product)
                        <div class="new-product-item swiper-slide">
                            <div class="product">
                                <div class="thumb">
                                    <a href="{{route('public.product.show', $product->id)}}" class="image">
                                        <img src="{{$product->photo[0]}}" alt="Product"/>
                                        <img class="hover-image" src="{{$product->photo[0]}}" alt="Product"/>
                                    </a>
                                    @if($product->discount > 0)
                                        <span class="badges">
                                            <span class="sale">{{$product->discount}} %</span>
                                        </span>
                                    @endif
                                    <div class="actions">
                                        <a class="action quickview" data-link-action="quickview"
                                           title="Quick view" data-bs-toggle="modal"
                                           data-bs-target="#modal-feature-{{$product->id}}"><i
                                                class="icon-size-fullscreen"></i></a>
                                    </div>
                                    <a href="{{route('public.cart.add', $product->id)}}" class="add-to-cart">Add To Cart
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="title"><a
                                            href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a>
                                    </h5>
                                    <span class="price">
                                                @if($product->discount > 0)
                                            <span
                                                class="new">${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                            <span class="old">${{sprintf("%.2f", $product->price)}}</span>
                                        @else
                                            <span class="new">${{sprintf("%.2f", $product->price)}}</span>
                                        @endif
                                        </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Add Arrows -->
                <div class="swiper-buttons">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- New Product End -->

    <!-- New Product Start -->
    <div class="section pb-100px" data-aos="fade-up" data-aos-delay="200">
        <div class="container">
            <!-- section title start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-left mb-11">
                        <h2 class="title">{{$sameProducts->count()}} Other Products In The Same Category:</h2>
                    </div>
                </div>
            </div>
            <!-- section title start -->
            <div class="new-product-slider swiper-container slider-nav-style-1" data-aos="fade-up" data-aos-delay="200">
                <div class="new-product-wrapper swiper-wrapper">
                    <!-- Single Product -->
                    @foreach($sameProducts as $product)
                        <div class="new-product-item swiper-slide">
                            <div class="product">
                                <div class="thumb">
                                    <a href="{{route('public.product.show', $product->id)}}" class="image">
                                        <img src="{{$product->photo[0]}}" alt="Product"/>
                                        <img class="hover-image" src="{{$product->photo[0]}}" alt="Product"/>
                                    </a>
                                    @if($product->discount > 0)
                                        <span class="badges">
                                            <span class="sale">{{$product->discount}} %</span>
                                        </span>
                                    @endif
                                    <div class="actions">
                                        <a class="action quickview" data-link-action="quickview"
                                           title="Quick view" data-bs-toggle="modal"
                                           data-bs-target="#modal-same-{{$product->id}}"><i
                                                class="icon-size-fullscreen"></i></a>
                                    </div>
                                                                                <a href="{{route('public.cart.add', $product->id)}}" class="add-to-cart">Add
                                                                                    To Cart
                                                                                </a>
                                </div>
                                <div class="content">
                                    <h5 class="title"><a
                                            href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a>
                                    </h5>
                                    <span class="price">
                                                @if($product->discount > 0)
                                            <span
                                                class="new">${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                            <span class="old">${{sprintf("%.2f", $product->price)}}</span>
                                        @else
                                            <span class="new">${{sprintf("%.2f", $product->price)}}</span>
                                        @endif
                                        </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Add Arrows -->
                <div class="swiper-buttons">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- New Product End -->
    @foreach($featureProducts as $product)
        <div class="modal fade" id="modal-feature-{{$product->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">x</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5 col-sm-12 col-xs-12 mb-lm-30px mb-sm-30px">
                                <!-- Swiper -->
                                <div class="swiper-container gallery-top gallery-top-feature-{{$product->id}} mb-4">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div
                                    class="swiper-container gallery-thumbs gallery-thumbs-feature-{{$product->id}} slider-nav-style-1 small-nav">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-buttons">
                                        <div
                                            class="swiper-button-next swiper-button-next-feature-{{$product->id}}"></div>
                                        <div
                                            class="swiper-button-prev swiper-button-prev-feature-{{$product->id}}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div class="product-details-content quickview-content">
                                    <h2>{{$product->title}}</h2>
                                    <p class="reference"></p>
                                    <div class="pro-details-rating-wrap">
                                        @if($product->countComments() > 0)
                                            <span class="read-review"><a class="reviews"
                                                                         href="{{route('public.product.show', $product->id)}}">Read {{$product->countComments() <= 1 ? 'review' : 'review(s)'}} {{$product->countComments()}}</a></span>
                                        @endif
                                    </div>
                                    <div class="pricing-meta">
                                        @if($product->discount > 0)
                                            <span>${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                            <span
                                                style="text-decoration: line-through;color: #999;">${{sprintf("%.2f", $product->price)}}</span>
                                        @else
                                            <span>${{sprintf("%.2f", $product->price)}}</span>
                                        @endif
                                    </div>
                                    <p class="quickview-para">{!! $product->description !!}
                                    </p>
                                    <div class="pro-details-quality">
                                        {!! Form::open(['url' => route('public.cart.add_qty',$product->id),'class'=>'pro-details-quality', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" type="text" name="quantity" value="1"/>
                                        </div>
                                        <div class="pro-details-cart btn-hover">
                                            {!! Form::button('Add
                                                To Cart', ['class' => 'add-cart btn btn-primary btn-hover-primary ml-4','type'=>'submit']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="pro-details-wish-com">
                                    </div>
                                    <div class="pro-details-social-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @foreach($sameProducts as $product)
        <div class="modal fade" id="modal-same-{{$product->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">x</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5 col-sm-12 col-xs-12 mb-lm-30px mb-sm-30px">
                                <!-- Swiper -->
                                <div class="swiper-container gallery-top gallery-top-same-{{$product->id}} mb-4">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div
                                    class="swiper-container gallery-thumbs gallery-thumbs-same-{{$product->id}} slider-nav-style-1 small-nav">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-next swiper-button-next-same-{{$product->id}}"></div>
                                        <div class="swiper-button-prev swiper-button-prev-same-{{$product->id}}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div class="product-details-content quickview-content">
                                    <h2>{{$product->title}}</h2>
                                    <p class="reference"></p>
                                    <div class="pro-details-rating-wrap">
                                        @if($product->countComments() > 0)
                                            <span class="read-review"><a class="reviews"
                                                                         href="{{route('public.product.show', $product->id)}}">Read {{$product->countComments() <= 1 ? 'review' : 'review(s)'}} {{$product->countComments()}}</a></span>
                                        @endif
                                    </div>
                                    <div class="pricing-meta">
                                        @if($product->discount > 0)
                                            <span>${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                            <span
                                                style="text-decoration: line-through;color: #999;">${{sprintf("%.2f", $product->price)}}</span>
                                        @else
                                            <span>${{sprintf("%.2f", $product->price)}}</span>
                                        @endif
                                    </div>
                                    <p class="quickview-para">{!! $product->description !!}
                                    </p>
                                    <div class="pro-details-quality">
                                        {!! Form::open(['url' => route('public.cart.add_qty',$product->id),'class'=>'pro-details-quality', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" type="text" name="quantity" value="1"/>
                                        </div>
                                        <div class="pro-details-cart btn-hover">
                                            {!! Form::button('Add
                                                To Cart', ['class' => 'add-cart btn btn-primary btn-hover-primary ml-4','type'=>'submit']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="pro-details-wish-com">
                                    </div>
                                    <div class="pro-details-social-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@section('script')
    @foreach($featureProducts as $product)
        <script>
            var galleryThumbFeature{{$product->id}} = new Swiper('.gallery-thumbs-feature-{{$product->id}}', {
                spaceBetween: 10,
                slidesPerView: 3,
                freeMode: true,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            });
            var galleryTopFeature{{$product->id}} = new Swiper('.gallery-top-feature-{{$product->id}}', {
                spaceBetween: 0,
                loop: true,
                navigation: {
                    nextEl: '.swiper-button-next-feature-{{$product->id}}',
                    prevEl: '.swiper-button-prev-feature-{{$product->id}}',
                },
                thumbs: {
                    swiper: galleryThumbFeature{{$product->id}}
                }
            });
        </script>
    @endforeach
    @foreach($sameProducts as $product)
        <script>
            var galleryThumbSame{{$product->id}} = new Swiper('.gallery-thumbs-same-{{$product->id}}', {
                spaceBetween: 10,
                slidesPerView: 3,
                freeMode: true,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            });
            var galleryTopSame{{$product->id}} = new Swiper('.gallery-top-same-{{$product->id}}', {
                spaceBetween: 0,
                loop: true,
                navigation: {
                    nextEl: '.swiper-button-next-same-{{$product->id}}',
                    prevEl: '.swiper-button-prev-same-{{$product->id}}',
                },
                thumbs: {
                    swiper: galleryThumbSame{{$product->id}}
                }
            });
        </script>
    @endforeach
@endsection

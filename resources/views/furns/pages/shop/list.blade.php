@extends('layouts.furns')
@section('content')
    <div class="shop-category-area pb-100px pt-70px">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-lg-last col-md-12 order-md-first">
                    <!-- Shop Top Area Start -->
                    <div class="shop-top-bar d-flex">
                        <!-- Left Side start -->
                        <p>There {{$pagination['total'] > 1 ? "are" : "is"}} {{$pagination['total']}}
                            Product{{$pagination['total'] > 1 ? "s" : ""}}.</p>
                        <!-- Left Side End -->
                        <br>
                        <br>
                    </div>
                    <!-- Shop Top Area End -->

                    <!-- Shop Bottom Area Start -->
                    <div class="shop-bottom-area">

                        <div class="row">
                            @foreach($products as $product)
                                <div class="col-lg-4  col-md-6 col-sm-6 col-xs-6" data-aos="fade-up"
                                     data-aos-delay="200">
                                    <!-- Single Product -->
                                    <div class="product mb-5">
                                        <div class="thumb">
                                            <a href="{{route('public.product.show', $product->id)}}" class="image">
                                                <img src="{{$product->photo[0]}}" alt="Product"/>
                                                <img class="hover-image" src="{{$product->photo[0]}}" alt="Product"/>
                                            </a>
                                            @if($product->discount > 0)
                                                <span class="badges">
                                            <span class="sale">{{$product->discount}} %</span>
                                        </span>
                                            @endif
                                            <div class="actions">
                                                <a class="action quickview" data-link-action="quickview"
                                                   title="Quick view" data-bs-toggle="modal"
                                                   data-bs-target="#modal-{{$product->id}}"><i
                                                        class="icon-size-fullscreen"></i></a>
                                            </div>
                                            <a href="{{route('public.cart.add', $product->id)}}" class="add-to-cart">Add
                                                To Cart
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="title"><a
                                                    href="{{route('public.product.show', $product->id)}}">{{$product->title}}</a>
                                            </h5>
                                            <span class="price">
                                                @if($product->discount > 0)
                                                    <span
                                                        class="new">${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2, PHP_ROUND_HALF_DOWN))}}</span>
                                                    <span class="old">${{sprintf("%.2f", $product->price)}}</span>
                                                @else
                                                    <span class="new">${{sprintf("%.2f", $product->price)}}</span>
                                                @endif
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <!--  Pagination Area Start -->

                        @if($pagination['prev_page_url'] !== null || $pagination['next_page_url'] !== null)
                            <div class="pro-pagination-style text-center mb-md-30px mb-lm-30px mt-6" data-aos="fade-up">
                                <ul>
                                    @if($pagination['prev_page_url'] !== null)
                                        <li>
                                            <a class="prev" href="{{$pagination['prev_page_url']}}"><i
                                                    class="ion-ios-arrow-left"></i></a>
                                        </li>
                                    @endif
                                    <li>
                                        <a class="active" href="#">{{$pagination['current_page']}}</a>
                                    </li>
                                    @if($pagination['next_page_url'] !== null)
                                        <li>
                                            <a href="{{$pagination['next_page_url']}}">{{$pagination['current_page'] + 1}}</a>
                                        </li>
                                    @endif

                                    @if($pagination['next_page_url'] !== null)
                                        <li>
                                            <a class="next" href="{{$pagination['next_page_url']}}"><i
                                                    class="ion-ios-arrow-right"></i></a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                    @endif
                    <!--  Pagination Area End -->
                    </div>
                    <!-- Shop Bottom Area End -->
                </div>
                <!-- Sidebar Area Start -->
                <div class="col-lg-3 order-lg-first col-md-12 order-md-last mb-md-60px mb-lm-60px">
                    <div class="shop-sidebar-wrap">
                        <!-- Sidebar single item -->
                        <div class="sidebar-widget">
                            <div class="main-heading">
                                <h3 class="sidebar-title">Category</h3>
                            </div>
                            <div class="sidebar-widget-category">
                                <ul>
                                    @foreach($categories as $category)
                                        @php
                                            $reqCategory = request('category', 0)
                                        @endphp
                                        <li>
                                            <a href="{{route('public.product.list') . ($category->id !== 0 ? "?category=$category->id" : '')}}" {{$reqCategory === $category ? 'class="selected"': ''}}>{{$category->category}}
                                                <span>({{$category->id === 0 ? $allCount : $category->product()->count()}})</span>
                                            </a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($products as $product)
        <div class="modal fade" id="modal-{{$product->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">x</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5 col-sm-12 col-xs-12 mb-lm-30px mb-sm-30px">
                                <!-- Swiper -->
                                <div class="swiper-container gallery-top gallery-top-{{$product->id}} mb-4">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div
                                    class="swiper-container gallery-thumbs gallery-thumbs-{{$product->id}} slider-nav-style-1 small-nav">
                                    <div class="swiper-wrapper">
                                        @foreach($product->photo as $photo)
                                            <div class="swiper-slide">
                                                <img class="img-responsive m-auto" src="{{$photo}}"
                                                     alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-next swiper-button-next-{{$product->id}}"></div>
                                        <div class="swiper-button-prev swiper-button-prev-{{$product->id}}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div class="product-details-content quickview-content">
                                    <h2>{{$product->title}}</h2>
                                    <p class="reference"></p>
                                    <div class="pro-details-rating-wrap">
                                        @if($product->countComments() > 0)
                                            <span class="read-review"><a class="reviews"
                                                                         href="{{route('public.product.show', $product->id)}}">Read {{$product->countComments() <= 1 ? 'review' : 'review(s)'}} {{$product->countComments()}}</a></span>
                                        @endif
                                    </div>
                                    <div class="pricing-meta">
                                        @if($product->discount > 0)
                                            <span>${{sprintf("%.2f", round((100 - $product->discount) / 100 * $product->price, 2))}}</span>
                                            <span
                                                style="text-decoration: line-through;color: #999;">${{sprintf("%.2f", $product->price)}}</span>
                                        @else
                                            <span>${{sprintf("%.2f", $product->price)}}</span>
                                        @endif
                                    </div>
                                    <p class="quickview-para">{!! $product->description !!}
                                    </p>
                                    <div class="pro-details-quality">
                                        {!! Form::open(['url' => route('public.cart.add_qty',$product->id),'class'=>'pro-details-quality', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" type="text" name="quantity" value="1"/>
                                        </div>
                                        <div class="pro-details-cart btn-hover">
                                            {!! Form::button('Add
                                                To Cart', ['class' => 'add-cart btn btn-primary btn-hover-primary ml-4','type'=>'submit']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="pro-details-wish-com">
                                    </div>
                                    <div class="pro-details-social-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@section('script')
    @foreach($products as $product)
        <script>
            var galleryThumb{{$product->id}} = new Swiper('.gallery-thumbs-{{$product->id}}', {
                spaceBetween: 10,
                slidesPerView: 3,
                freeMode: true,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            });
            var galleryTop{{$product->id}} = new Swiper('.gallery-top-{{$product->id}}', {
                spaceBetween: 0,
                loop: true,
                navigation: {
                    nextEl: '.swiper-button-next-{{$product->id}}',
                    prevEl: '.swiper-button-prev-{{$product->id}}',
                },
                thumbs: {
                    swiper: galleryThumb{{$product->id}}
                }
            });
        </script>
    @endforeach
@endsection

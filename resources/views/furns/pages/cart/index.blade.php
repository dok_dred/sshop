@extends('layouts.furns')

@section('content')
    @if($cart['qty'] === 0)
        <!-- Cart area start -->
        <div class="empty-cart-area pb-100px pt-100px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cart-heading">
                            <h2>Your cart item</h2>
                        </div>
                        <div class="empty-text-contant text-center">
                            <i class="icon-handbag"></i>
                            <h1>There are no more items in your cart</h1>
                            <a class="empty-cart-btn" href="{{route('public.product.list')}}">
                                <i class="ion-ios-arrow-left"> </i> Continue shopping
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Cart area end -->
    @else
        <!-- Cart Area Start -->
        <div class="cart-main-area pt-100px pb-100px"a>
            <div class="container">
                <h3 class="cart-page-title">Your cart items</h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        {!! Form::open(['url' => route('public.cart.update'), 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                        <div class="table-content table-responsive cart-table-content">
                            <table>
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th>Until Price</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart['items'] as $item)
                                    @php
                                        $product = $cart['products'][$item['id']];
                                    @endphp
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="{{route('public.product.show', $product->id)}}"><img
                                                    class="img-responsive ml-3" src="{{$product->photo[0]}}"
                                                    alt=""/></a>
                                        </td>
                                        <td class="product-name"><a href="#">{{$product->title}}</a></td>
                                        <td class="product-price-cart">
                                            @if($product->priceWithAllDiscount() !== $product->price)
                                                <span>${{sprintf("%.2f", $product->priceWithAllDiscount())}}</span>
                                                <span
                                                    style="text-decoration: line-through;color: #999;">${{sprintf("%.2f", $product->price)}}</span>
                                            @else
                                                <span>${{sprintf("%.2f", $product->price)}}</span>
                                            @endif
                                        </td>
                                        <td class="product-quantity">
                                            <div class="cart-plus-minus">
                                                <input class="cart-plus-minus-box" type="text"
                                                       name="product-{{$product->id}}" value="{{$item['qty']}}"/>
                                            </div>
                                        </td>
                                        <td class="product-subtotal">
                                            <span>${{sprintf("%.2f", $product->priceWithAllDiscount() * $item['qty'])}}</span>
                                        <td class="product-remove">
                                            <a href="{{route('public.cart.delete', $item['__raw_id'])}}"><i
                                                    class="icon-close"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="cart-shiping-update-wrapper">
                                    <div class="cart-shiping-update">
                                        <a href="{{route('public.product.list')}}">Continue Shopping</a>
                                    </div>
                                    <div class="cart-clear">
                                        {!! Form::button('Update Shopping Cart', ['type'=>'submit']) !!}
                                        <a href="{{route('public.cart.remove.all')}}">Clear Shopping Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="row">
                            @php
                                $coupon = Cookie::get('code', '');
                                $discount = Cookie::get('discount', 0)
                            @endphp
                            <div class="col-lg-4 col-md-6 mb-lm-30px">
                            </div>
                            <div class="col-lg-4 col-md-6 mb-lm-30px">
                                <div class="discount-code-wrapper">
                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gray">Use Coupon Code</h4>
                                    </div>
                                    <div class="discount-code">
                                        @if($coupon !== '')
                                            <p>Your coupon code - <b>{{$coupon}}</b></p>
                                            {!! Form::open(['url' => route('public.cart.promocode.deactivate'), 'enctype'=>'multipart/form-data']) !!}
                                            {!! Form::button('Clear coupon', ['type'=>'submit', 'class' => 'cart-btn-2']) !!}
                                            {!! Form::close() !!}
                                        @else
                                            <p>Enter your coupon code if you have one.</p>
                                            {!! Form::open(['url' => route('public.cart.promocode.activate'), 'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                                            <input type="text" required="" name="code"/>
                                            {!! Form::button('Apply Coupon', ['type'=>'submit', 'class' => 'cart-btn-2']) !!}
                                            {!! Form::close() !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 mt-md-30px">
                                <div class="grand-totall">
                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
                                    </div>
                                    <h5>Total products
                                        <span>${{sprintf("%.2f", $cart['total_without_discount'])}}</span>
                                    </h5>

                                    <div class="total-shipping">
                                        @if($coupon !== '')
                                            <h5>Used coupon <b> {{$coupon}} </b><span>{{$discount}}%</span>
                                            </h5>
                                        @endif
                                        <br>
                                        <h5>All discount
                                            <span>${{sprintf("%.2f",$cart['total_without_discount'] - $cart['total'])}}</span>
                                        </h5>
                                    </div>
                                    <h4 class="grand-totall-title">Grand Total
                                        <span>${{sprintf("%.2f", $cart['total'])}}</span></h4>
                                    <a href="{{route('public.cart.checkout')}}">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Cart Area End -->
    @endif
@endsection

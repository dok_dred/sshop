@extends('layouts.furns')
@section('content')
    <!-- checkout area start -->
    <div class="checkout-area pt-100px pb-100px">
        <div class="container">
            <div class="row">
                <div class="offset-3 col-lg-6 mt-md-30px mt-lm-30px ">
                    <div class="your-order-area">
                        <h3>Your order</h3>
                        <div class="your-order-wrap gray-bg-4">
                            <div class="your-order-product-info">
                                <div class="your-order-top">
                                    <ul>
                                        <li>Product</li>
                                        <li>Total</li>
                                    </ul>
                                </div>
                                <div class="your-order-middle">
                                    <ul>
                                        @foreach($cart['items'] as $item)
                                            @php
                                                $product = $cart['products'][$item['id']];
                                            @endphp
                                            <li><span
                                                    class="order-middle-left">{{$product->title}} X {{$item['qty']}}</span>
                                                <span
                                                    class="order-price">${{sprintf("%.2f", $product->priceWithAllDiscount() * $item['qty'])}}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="your-order-bottom">
                                    {{--<ul>
                                            <li class="your-order-shipping">Shipping</li>
                                            <li>Free shipping</li>
                                        </ul>--}}
                                    @php
                                        $coupon = Cookie::get('code', '');
                                        $discount = Cookie::get('discount', 0)
                                    @endphp
                                    @if($coupon !== '')
                                        <ul>
                                            <li>Used coupon <b> {{$coupon}} </b><span>{{$discount}}%</span></li>
                                        </ul>
                                    @endif
                                </div>
                                <div class="your-order-total">
                                    <ul>
                                        <li class="order-total">Total</li>
                                        <li>${{sprintf("%.2f", $cart['total'])}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="payment-method">
                                {{--<div class="payment-accordion element-mrg">
                                    <div id="faq" class="panel-group">
                                        <div class="panel panel-default single-my-account m-0">
                                            <div class="panel-heading my-account-title">
                                                <h4 class="panel-title"><a data-bs-toggle="collapse"
                                                                           href="#my-account-1" class="collapsed"
                                                                           aria-expanded="true">Direct bank transfer</a>
                                                </h4>
                                            </div>
                                            <div id="my-account-1" class="panel-collapse collapse show"
                                                 data-bs-parent="#faq">

                                                <div class="panel-body">
                                                    <p>Please send a check to Store Name, Store Street, Store Town,
                                                        Store State / County, Store Postcode.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default single-my-account m-0">
                                            <div class="panel-heading my-account-title">
                                                <h4 class="panel-title"><a data-bs-toggle="collapse"
                                                                           href="#my-account-2" aria-expanded="false"
                                                                           class="collapsed">Check payments</a></h4>
                                            </div>
                                            <div id="my-account-2" class="panel-collapse collapse"
                                                 data-bs-parent="#faq">

                                                <div class="panel-body">
                                                    <p>Please send a check to Store Name, Store Street, Store Town,
                                                        Store State / County, Store Postcode.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default single-my-account m-0">
                                            <div class="panel-heading my-account-title">
                                                <h4 class="panel-title"><a data-bs-toggle="collapse"
                                                                           href="#my-account-3">Cash on delivery</a>
                                                </h4>
                                            </div>
                                            <div id="my-account-3" class="panel-collapse collapse"
                                                 data-bs-parent="#faq">

                                                <div class="panel-body">
                                                    <p>Please send a check to Store Name, Store Street, Store Town,
                                                        Store State / County, Store Postcode.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                        <div class="Place-order mt-25 offset-2 col-8">
                            @if($sessionId !== '')
                                <a class="btn-hover" id="checkout-button" href="#">Pay via card</a>
                                <br>
                                @auth
                                    @if(Auth::user()->getValet()->isVerified())
                                        <a href="{{route('public.payment.bank')}}" class="btn-hover">pay via bank</a>
                                    @else
                                        <a href="{{route('public.account.account')}}" class="btn-hover">Verify bank</a>
                                    @endif
                                @endauth
                                @guest
                                    <a href="{{route('public.account.account')}}" class="btn-hover">Register for paying via bank</a>
                                @endguest
                                <div class="row text-center"><span><b>OR</b></span></div>
                            @endif
                            <a href="{{route('public.product.list')}}" class="btn-hover">Continue shopping</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- checkout area end -->
@endsection
@section('script')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var stripe = Stripe('{{$stripePk}}');
        var checkoutButton = document.getElementById('checkout-button');

        checkoutButton.addEventListener('click', function () {
            stripe.redirectToCheckout({
                sessionId: '{{$sessionId}}'
            }).then(function (result) {
                console.log(result)
                // If `redirectToCheckout` fails due to a browser or network
                // error, display the localized error message to your customer
                // using `result.error.message`.
            });
        });
    </script>
@endsection

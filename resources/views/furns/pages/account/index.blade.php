@extends('layouts.furns')

@section('content')
    <section id="cart">
        @if (session('success'))
            <div class="alert alert-success mt-5">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger mt-5">
                {{ session('error') }}
            </div>
        @endif
        @guest
        <!-- login area start -->
            <div class="login-register-area pt-100px pb-100px">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                            <div class="login-register-wrapper">
                                <div class="login-register-tab-list nav">
                                    <a class="active" data-bs-toggle="tab" href="#lg1">
                                        <h4>login</h4>
                                    </a>
                                    <a data-bs-toggle="tab" href="#lg2">
                                        <h4>register</h4>
                                    </a>
                                </div>
                                <div class="tab-content">
                                    <div id="lg1" class="tab-pane active">
                                        <div class="login-form-container">
                                            <div class="login-register-form">
                                                <form action="{{ route('login')}}" method="post">
                                                    @csrf
                                                    <input type="text" name="email" placeholder="Email"/>
                                                    <input type="password" name="password" placeholder="Password"/>
                                                    <div class="button-box">
                                                        <button type="submit"><span>Login</span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="lg2" class="tab-pane">
                                        <div class="login-form-container">
                                            <div class="login-register-form">
                                                <form action="{{ route('register') }}" method="post">
                                                    @csrf
                                                    <input type="text" name="name" placeholder="Username"/>
                                                    <input type="password" name="password" placeholder="Password"/>
                                                    <input name="email" placeholder="Email" type="email"/>
                                                    <input type="password" name="password_confirmation"
                                                           placeholder="Confirm password"/>
                                                    <div class="button-box">
                                                        <button type="submit"><span>Register</span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login area end -->
        @endguest

        @auth
            @php
                $valet = Auth::user()->getValet();
            @endphp
            <div class="mt-5 container">
                @if ($valet->isFilled())
                    <div class="table-responsive text-center">
                        <table class="mt-5 table table-hover table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">Routing number</th>
                                <th class="text-center">Account number</th>
                                <th class="text-center">Holder name</th>
                                <th class="text-center">Verified</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $valet->getRoutingNumber() }}</td>
                                <td>{{ $valet->getAccountNumber() }}</td>
                                <td>{{ $valet->getAccountHolderName() }}</td>
                                <td>{{ $valet->isVerified() ? 'Yes' : 'No' }}</td>
                                <td><a href="{{route('public.account.clear')}}">Clear</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <br><br>
                    @if(!$valet->isVerified())
                        <div class="row">
                            @if(!$valet->isApproved())
                                <div class="col-lg-2 text-center" style="margin-top: 1em;">
                                    <a class="btn btn-info" id="approve" style="display: none"
                                       href="{{ route('public.account.approve') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('token-form').submit();">
                                        Approve account
                                    </a>

                                    <a class="btn btn-info" id="tokenizer"
                                       onclick="event.preventDefault();
                                           var stripe = Stripe('{{$stripePk}}');
                                           token = '';
                                           bankAccount = '';
                                           stripe
                                           .createToken('bank_account', {
                                           country: 'US',
                                           currency: 'usd',
                                           routing_number: '{{ $valet->getRoutingNumber() }}',
                                           account_number: '{{$valet->getAccountNumber()}}',
                                           account_holder_name: '{{ $valet->getAccountHolderName() }}',
                                           account_holder_type: '{{ $valet->getAccountHolderType()}}',
                                           })
                                           .then(function(result) {
                                           bankAccount = result.token.bank_account.id;
                                           token = result.token.id;
                                           // Handle result.error or result.token
                                           console.log(result.token.bank_account.id);

                                           document.getElementById('tokenizer').style.display = 'none';
                                           document.getElementById('approve').style.display = '';
                                           document.getElementById('ba_token').value = token;
                                           document.getElementById('bank_account').value = bankAccount;
                                           console.log(document.getElementById('ba_token').value);
                                           });

                                           ">
                                        Set token
                                    </a>

                                    <form id="token-form" action="{{ route('public.account.approve') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                        <input type="hidden" id="ba_token" name="token" value="">
                                        <input type="hidden" id="bank_account" name="bank_account" value="">
                                    </form>
                                </div>
                            @else
                                <div class="login-register-area pt-100px pb-100px">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                                                <div class="login-register-wrapper">
                                                    <div class="login-register-tab-list nav">
                                                        <a data-bs-toggle="tab" href="#lg1">
                                                            <h4>Verify account by small deposits</h4>
                                                            <p>Check your account and enter deposits from our shop</p>
                                                        </a>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div id="lg1" class="tab-pane active">
                                                            <div class="login-form-container">
                                                                <div class="login-register-form">
                                                                    <form action="{{ route('public.account.verify') }}" method="post">
                                                                        @csrf
                                                                            <input type="text" name="first" class="form-control" id="firstsecond"
                                                                                   placeholder="00">

                                                                            <input type="text" name="second" class="form-control" id="second"
                                                                                   placeholder="11">
                                                                        <div class="button-box">
                                                                            <button type="submit"><span>Verify</span></button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endif
                @else
                    <div class="login-register-area pt-100px pb-100px">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                                    <div class="login-register-wrapper">
                                        <div class="login-register-tab-list nav">
                                            <a class="" data-bs-toggle="tab" href="#lg1">
                                                <h4>Add new bank</h4>
                                            </a>
                                        </div>
                                        <div class="tab-content">
                                            <div id="lg1" class="tab-pane active">
                                                <div class="login-form-container">
                                                    <div class="login-register-form">
                                                        <form action="{{ route('public.account.add') }}" method="post">
                                                            @csrf
                                                            <input type="text" name="routing_number"
                                                                   class="form-control" id="routing_number"
                                                                   placeholder="110000000">
                                                            <input type="text" name="account_number"
                                                                   class="form-control" id="account_number"
                                                                   placeholder="000123456789">
                                                            <input type="text" name="account_holder_name"
                                                                   class="form-control" id="account_holder_name"
                                                                   placeholder="John Smith">
                                                            <div class="button-box">
                                                                <button type="submit"><span>Add</span></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        @endauth
    </section>
@endsection
@section('script')
    <script src="https://js.stripe.com/v3/"></script>
@endsection

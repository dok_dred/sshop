<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <title>{{$title}}</title>
    <meta name="description"
          content="240+ Best Bootstrap Templates are available on this website. Find your template for your project compatible with the most popular HTML library in the world."/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="canonical" href="https://htmldemo.hasthemes.com/furns/"/>

    <!-- Open Graph (OG) meta tags are snippets of code that control how URLs are displayed when shared on social media  -->
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$title}}"/>
    <meta property="og:url" content="PAGE_URL"/>
    <meta property="og:site_name" content="{{config('app.name', "Laravel")}}"/>
    <!-- For the og:image content, replace the # with a link of an image -->
    <meta property="og:image" content="#"/>
    <meta property="og:description" content="{{config('app.name', "Laravel")}}"/>
    <meta name="robots" content="noindex, follow"/>
    <!-- Add site Favicon -->
    <link rel="icon" href="{{ asset('assets/furns/images/favicon/favicon.png') }}" sizes="32x32"/>
    <link rel="icon" href="{{ asset('assets/furns/images/favicon/favicon.png') }}" sizes="192x192"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/furns/images/favicon/favicon.png') }}"/>
    <meta name="msapplication-TileImage" content="{{ asset('assets/furns/images/favicon/favicon.png') }}"/>
    <!-- Structured Data  -->
    <script type="application/ld+json">
        {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{ config('app.name', "Laravel") }}",
        "url": "{{ config('app.url', "https://localhost:8080") }}"
        }
    </script>

    <!-- vendor css (Bootstrap & Icon Font) -->
    <!--
        <link rel="stylesheet" href="assets/css/vendor/simple-line-icons.css" />
        <link rel="stylesheet" href="assets/css/vendor/ionicons.min.css" />
    -->

    <!-- plugins css (All Plugins Files) -->
    <!--
         <link rel="stylesheet" href="assets/css/plugins/animate.css" />
        <link rel="stylesheet" href="assets/css/plugins/swiper-bundle.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/jquery.lineProgressbar.css">
        <link rel="stylesheet" href="assets/css/plugins/nice-select.css" />
        <link rel="stylesheet" href="assets/css/plugins/venobox.css" /> -->

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <link rel="stylesheet" href="{{ asset("assets/furns/css/vendor/vendor.min.css") }}"/>
    <link rel="stylesheet" href="{{ asset("assets/furns/css/plugins/plugins.min.css") }}"/>
    <link rel="stylesheet" href="{{ asset("assets/furns/css/style.min.css") }}"/>

    <!-- Main Style -->
    <link rel="stylesheet" href="{{ asset("assets/furns/css/style.css") }}"/>

</head>
<body>

@include("furns.components.header")

<div class="offcanvas-overlay"></div>

@if(Route::currentRouteName() !== 'public.home')
    @include('furns.components.page.breadcrumb')
@endif


@yield("content")

<!-- Footer Area Start -->
@include("furns.components.footer")
<!-- Footer Area End -->

<!-- Global Vendor, plugins JS -->

<!-- Vendor JS -->
<!--
<script src="assets/js/vendor/jquery-3.5.1.min.js"></script>
<script src="assets/js/vendor/popper.min.js"></script>
<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/vendor/jquery-migrate-3.3.0.min.js"></script>
<script src="assets/js/vendor/modernizr-3.11.2.min.js"></script>
-->

<!--Plugins JS-->
<!--
<script src="assets/js/plugins/swiper-bundle.min.js"></script>
<script src="assets/js/plugins/jquery-ui.min.js"></script>
<script src="assets/js/plugins/jquery.nice-select.min.js"></script>
<script src="assets/js/plugins/countdown.js"></script>
<script src="assets/js/plugins/scrollup.js"></script>
<script src="assets/js/plugins/jquery.waypoints.js"></script>
<script src="assets/js/plugins/jquery.lineProgressbar.js"></script>
<script src="assets/js/plugins/jquery.zoom.min.js"></script>
<script src="assets/js/plugins/venobox.min.js"></script>
<script src="assets/js/plugins/ajax-mail.js"></script>
-->

<!-- Use the minified version files listed below for better performance and remove the files listed above -->
<script src="{{ asset("assets/furns/js/vendor/vendor.min.js") }}"></script>
<script src="{{ asset("assets/furns/js/plugins/plugins.min.js") }}"></script>

<!-- Main Js -->
<script src="{{ asset("assets/furns/js/main.js") }}"></script>
@yield('script')
</body>
</html>

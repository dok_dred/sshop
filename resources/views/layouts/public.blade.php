<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$title}}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/public.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/owl.transitions.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/loader.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/jquery.nstSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/cubeportfolio.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/main/css/bootsnav.css') }}" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    <!--Loader-->
    <div class="loader" style="display: none;">
        <div class="spinner-load">
            <div class="dot1">
            </div>
            <div class="dot2">
            </div>
        </div>
    </div>
    <!--HEADER-->
    <header>
        <div class="wrap-sticky" style="height: 79px;">
            @include('public.components.header.navbar')
        </div>
    </header>
    <!--Footer-->
    <section class="grid padding">
        <div class="container">
            @yield("content")
        </div>
    </section>

    <footer class="padding_top bottom_half">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer_panel content_space">
                        <h2 class="heading_border heading_space">{{config('app.name', "Laravel")}}</h2>
                        <h4><a href="/privacy-policy">Privacy Policy</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <p>Copyright © {{date('Y')}}}
                        <a href="/"> {{config('app.name', "Laravel")}}
                        </a>. All Right Reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!-- Scripts -->
<script src="{{ asset('assets/main/js/jquery-2.2.3.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"></script>
<script src="{{ asset('assets/main/js/gmap3.min.js') }}"></script>
<script src="{{ asset('assets/main/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/main/js/bootsnav.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.appear.js') }}"></script>
<script src="{{ asset('assets/main/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.cubeportfolio.min.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('assets/main/js/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/main/js/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/main/js/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets/main/js/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/main/js/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset('assets/main/js/kinetic.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.final-countdown.js') }}"></script>
<script src="{{ asset('assets/main/js/functions.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery-countTo.js') }}"></script>
<script src="{{ asset('js/public.js') }}"></script>

</html>

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'middleware' => ['auth', 'adm']
    ],
    function () {
        Route::get('/', 'Admin\MainController@index')->name('index');
        Route::post('/upload', 'Admin\MainController@upload')->name('upload');
        Route::group(
            [
                'prefix' => 'category',
                'as' => 'categories.',
            ],
            function () {
                Route::get('/', 'Admin\CategoryController@index')->name('list');
                Route::post('/', 'Admin\CategoryController@store')->name('store');
                Route::get('/create', 'Admin\CategoryController@create')->name('create');
                Route::post('/update/{category}', 'Admin\CategoryController@update')->name('update');
                Route::post('/delete/{category}', 'Admin\CategoryController@destroy')->name('delete');
                Route::get('/{category}', 'Admin\CategoryController@edit')->name('edit');
            }
        );

        Route::group(
            [
                'prefix' => 'page',
                'as' => 'pages.',
            ],
            function () {
                Route::get('/', 'Admin\PageController@index')->name('list');
                Route::post('/', 'Admin\PageController@store')->name('store');
                Route::get('/create', 'Admin\PageController@create')->name('create');
                Route::post('/update/{page}', 'Admin\PageController@update')->name('update');
                Route::post('/delete/{page}', 'Admin\PageController@destroy')->name('delete');
                Route::get('/{page}', 'Admin\PageController@edit')->name('edit');
            }
        );

        Route::group(
            [
                'prefix' => 'product',
                'as' => 'products.',
            ],
            function () {
                Route::get('/', 'Admin\ProductController@index')->name('list');
                Route::post('/', 'Admin\ProductController@store')->name('store');
                Route::get('/create', 'Admin\ProductController@create')->name('create');
                Route::post('/update/{product}', 'Admin\ProductController@update')->name('update');
                Route::post('/delete/{product}', 'Admin\ProductController@destroy')->name('delete');
                Route::get('/comment/{product}/delete/{comment}', 'Admin\ProductController@deleteComment')->name(
                    'comment.delete'
                );
                Route::get('/{product}', 'Admin\ProductController@edit')->name('edit');
            }
        );


        Route::group(
            [
                'prefix' => 'menu',
                'as' => 'menu.',
            ],
            function () {
                Route::get('/', 'Admin\MenuController@index')->name('list');
                Route::post('/', 'Admin\MenuController@store')->name('store');
                Route::get('/create', 'Admin\MenuController@create')->name('create');
                Route::post('/update/{menu}', 'Admin\MenuController@update')->name('update');
                Route::post('/delete/{menu}', 'Admin\MenuController@destroy')->name('delete');
                Route::get('/{menu}', 'Admin\MenuController@edit')->name('edit');
            }
        );

        Route::group(
            [
                'prefix' => 'users',
                'as' => 'users.',
            ],
            function () {
                Route::get('/', 'Admin\UserController@index')->name('index');
                Route::get('/set/{user]', 'Admin\UserController@setAdmin')->name('set');
                Route::get('/unset/{user]', 'Admin\UserController@unsetAdmin')->name('unset');
                Route::get('/inset/{user]', 'Admin\UserController@delete')->name('delete');
            }
        );

        Route::group(
            [
                'prefix' => 'config',
                'as' => 'config.',
            ],
            function () {
                Route::get('/', 'Admin\ConfigController@index')->name('list');
                Route::post('/', 'Admin\ConfigController@store')->name('store');
                Route::get('/create', 'Admin\ConfigController@create')->name('create');
                Route::post('/update/{cfg}', 'Admin\ConfigController@update')->name('update');
                Route::post('/delete/{cfg}', 'Admin\ConfigController@destroy')->name('delete');
                Route::get('/{cfg}', 'Admin\ConfigController@edit')->name('edit');
            }
        );

        Route::group(
            [
                'prefix' => 'promocodes',
                'as' => 'promocodes.',
            ],
            function () {
                Route::get('/', 'Admin\PromoCodeController@index')->name('list');
                Route::post('/', 'Admin\PromoCodeController@store')->name('store');
                Route::get('/create', 'Admin\PromoCodeController@create')->name('create');
                Route::post('/update/{promoCode}', 'Admin\PromoCodeController@update')->name('update');
                Route::post('/delete/{promoCode}', 'Admin\PromoCodeController@destroy')->name('delete');
                Route::get('/{promoCode}', 'Admin\PromoCodeController@edit')->name('edit');
            }
        );
    }
);

Route::group(
    ['as' => 'public.'],
    function () {
        Route::get('/', 'Pub\PageController@home')->name('home');

        Route::group(
            ['as' => 'product.', 'prefix' => 'products'],
            function () {
                Route::get('/', 'Pub\ProductController@index')->name('list');
                Route::get('/{product}', 'Pub\ProductController@show')->name('show');
                Route::post('/comment/{product}/add', 'Pub\ProductController@addComment')->name('comment.add');
            }
        );

        Route::group(
            ['as' => 'cart.', 'prefix' => 'cart'],
            function () {
                Route::get('/', 'Pub\CartController@index')->name('index');
                Route::get('/checkout', 'Pub\CartController@checkout')->name('checkout');
                Route::get('/add/{product}', 'Pub\CartController@add')->name('add');
                Route::post('/add/{product}', 'Pub\CartController@add')->name('add_qty');
                Route::get('/delete/{itemId}', 'Pub\CartController@delete')->name('delete');
                Route::get('/remove/all', 'Pub\CartController@removeAll')->name('remove.all');
                Route::get('/remove/{itemId}', 'Pub\CartController@removeItem')->name('remove.item');
                Route::post('/update', 'Pub\CartController@updateCart')->name('update');
                Route::post('/promocode/activate', 'Pub\CartController@activateCode')->name('promocode.activate');
                Route::post('/promocode/deactivate', 'Pub\CartController@deactivateCode')->name('promocode.deactivate');
            }
        );

        Route::group(
            ['as' => 'payment.', 'prefix' => 'payment'],
            function () {
                Route::get('/success', 'Pub\CartController@success')->name('success');
                Route::get('/cancel', 'Pub\CartController@cancel')->name('cancel');
                Route::get('/bank', 'Pub\CartController@bankPay')->name('bank');
            }
        );

        Route::group(
            ['as' => 'account.', 'prefix' => 'account'],
            function () {
                Route::get('/', 'Pub\AccountController@account')->name('account');
                Route::post('/approve', 'Pub\AccountController@approve')->name('approve');
                Route::post('/add', 'Pub\AccountController@add')->name('add');
                Route::post('/verify', 'Pub\AccountController@verify')->name('verify');
                Route::get('/clear', 'Pub\AccountController@clear')->name('clear');
            }
        );

        Route::get('/{page}', 'Pub\PageController@show')->name('page');
    }
);




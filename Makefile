e:
	docker-compose exec php-cli $(c)

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose up --build -d

ps:
	docker-compose ps

test:
	docker-compose exec php-cli vendor/bin/phpunit

migrate:
	docker-compose exec php-cli php artisan migrate

assets-install:
	docker-compose exec node yarn install

assets-rebuild:
	docker-compose exec node npm rebuild node-sass --force

assets-dev:
	docker-compose exec node yarn run dev

assets-watch:
	docker-compose exec node yarn run watch

queue:
	docker-compose exec php-cli php artisan queue:work

horizon:
	docker-compose exec php-cli php artisan horizon

horizon-pause:
	docker-compose exec php-cli php artisan horizon:pause

horizon-continue:
	docker-compose exec php-cli php artisan horizon:continue

horizon-terminate:
	docker-compose exec php-cli php artisan horizon:terminate

memory:
	sudo sysctl -w vm.max_map_count=262144

docker-destroy:
	docker-compose down -v --remove-orphans

perm:
	sudo chgrp -R www-data storage public bootstrap/cache
	sudo chmod -R 777 storage public bootstrap/cache

<?php

namespace App;

use App\Model\Valet;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use phpDocumentor\Reflection\Types\Void_;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property int $admin
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAdmin($value)
 * @property string $routing_number
 * @property string $account_number
 * @property string $account_holder_name
 * @property string $account_holder_type
 * @property string $ba_token
 * @property string $cus_token
 * @property int $valet_verified
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAccountHolderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAccountHolderType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBaToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCusToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoutingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereValetVerified($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $attributes = [
        'admin' => false,
        'routing_number' => '',
        'account_number' => '',
        'account_holder_name' => '',
        'account_holder_type' => 'individual',
        'ba_token' => '',
        'cus_token' => '',
        'valet_verified' => false,
    ];

    public function setAdmin(): void
    {
        $this->admin = true;
    }

    public function unsetAdmin(): void
    {
        $this->admin = false;
    }

    public function isAdmin(): bool
    {
        return $this->admin;
    }

    public function setValet(Valet $valet): void
    {
        $this->routing_number = $valet->getRoutingNumber() ?? '';
        $this->account_number = $valet->getAccountNumber() ?? '';
        $this->account_holder_name = $valet->getAccountHolderName() ?? '';
        $this->account_holder_type = 'individual';
        $this->ba_token = $valet->getBaToken() ?? '';
        $this->cus_token = $valet->getCusToken() ?? '';
        $this->valet_verified = $valet->isVerified();
    }

    public function clearValet(): void
    {
        $this->routing_number = '';
        $this->account_number = '';
        $this->account_holder_name = '';
        $this->ba_token = '';
        $this->cus_token = '';
        $this->valet_verified = false;
    }

    /**
     * @return int
     */
    public function getValet(): Valet
    {
        $valet =  new Valet();
        $valet->setAccountHolderName($this->account_holder_name);
        $valet->setAccountHolderType($this->account_holder_type);
        $valet->setAccountNumber($this->account_number);
        $valet->setRoutingNumber($this->routing_number);
        $valet->setValetVerified($this->valet_verified);
        $valet->setBaToken($this->ba_token);
        $valet->setCusToken($this->cus_token);

        return $valet;
    }
}

<?php


namespace App\Service;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class UploaderService
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage::disk('public');
    }

    public function upload(UploadedFile $file): array
    {
        $response =
            [
                "fileName" => "",
                "uploaded" => false,
                "url" => ""
            ];

        $image = $file->getClientOriginalName();
        if ($this->storage->exists('/img/' . $image)) {
            $image = uniqid() . '_' . $image;
        }
        $file = $file->move(public_path() . '/img/', $image);
        $response['url'] = URL::to('/img/' . $file->getFilename());
        $response['uploaded'] = true;
        $response['fileName'] = $file->getFilename();

        return $response;
    }
}

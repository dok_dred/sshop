<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Model\PromoCode
 *
 * @property int $id
 * @property string $code
 * @property int $discount
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereId($value)
 * @mixin \Eloquent
 */
class PromoCode extends Model
{
    public $timestamps = false;
    protected $table = 'promocode';

}

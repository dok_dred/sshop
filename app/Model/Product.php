<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Cookie;


/**
 * App\Model\Product
 *
 * @property int $id
 * @property string $title
 * @property float $price
 * @property string $photo
 * @property int $category_id
 * @property string $description
 * @property array $reviews
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Model\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereReviews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Product whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int $discount
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDiscount($value)
 */
class Product extends Model
{
    protected $casts = [
        'reviews' => 'array',
        'photo' => 'array'
    ];
    protected $attributes = [
        'reviews' => '{}'
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function addComment(array $comment): void
    {
        $reviews = $this->reviews;
        $reviews[$comment['id']] = $comment;
        $this->reviews = $reviews;
    }

    public function deleteComment(string $commentId): void
    {
        $reviews = $this->reviews;
        unset($reviews[$commentId]);
        $this->reviews = $reviews;
    }

    public function countComments() : int
    {
        return count($this->reviews);
    }

    public function priceWithAllDiscount(): float
    {
        $couponDiscount = Cookie::get('discount', 0);

        return round((100 - $this->discount - $couponDiscount) / 100 * $this->price, 2);
    }
}

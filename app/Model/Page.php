<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

/**
 * App\Model\Page
 *
 * @property int $id
 * @property string $path
 * @property string $title
 * @property string $content
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    public function resolveRouteBinding($value, $field = null)
    {
        if (!Route::currentRouteNamed("admin.*")) {
            return $this->where('path', $value)->firstOrFail();
        } else {
            return $this->where('id', $value)->firstOrFail();
        }
    }
}

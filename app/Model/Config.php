<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Config
 *
 * @property int $id
 * @property string $name
 * @property string|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|Config newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Config newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Config query()
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereValue($value)
 * @mixin \Eloquent
 * @property string $key
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereKey($value)
 */
class Config extends Model
{
    use HasFactory;
    public $timestamps = false;

    public static function getValue($key = '')
    {
        $config = Config::where('key', $key)->first();
        if ($config) {
            return $config->value;
        }
        return '';
    }
}

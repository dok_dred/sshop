<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Menu
 *
 * @property int $id
 * @property string $path
 * @property string $name
 * @property int $parent_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Menu query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Menu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Menu wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereParentId($value)
 * @property-read int|null $children_count
 * @property-read Menu $parent
 * @property \Illuminate\Database\Eloquent\Collection|Menu[] $children
 * @property int $sort
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereSort($value)
 */
class Menu extends Model
{
    public $timestamps = false;
    protected $table = 'menu';

    public static function tree()
    {
        $items = Menu::all();
        $grouped = $items->groupBy('parent_id');

        foreach ($items as $item) {
            if ($grouped->has($item->id)) {
                $item->children = $grouped[$item->id];
            }
        }

        return $items->where('parent_id', null);
    }

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }
}

<?php


namespace App\Model;


class Valet
{
    private $routingNumber;
    private $accountNumber;
    private $accountHolderName;
    private $accountHolderType;
    private $baToken;
    private $cusToken;
    private $valetVerified;

    /**
     * @return mixed
     */
    public function getValetVerified()
    {
        return $this->valetVerified;
    }

    /**
     * @param mixed $valetVerified
     */
    public function setValetVerified($valetVerified): void
    {
        $this->valetVerified = $valetVerified;
    }

    /**
     * @return mixed
     */
    public function getCusToken()
    {
        return $this->cusToken;
    }

    /**
     * @param mixed $cusToken
     */
    public function setCusToken($cusToken): void
    {
        $this->cusToken = $cusToken;
    }

    /**
     * @return mixed
     */
    public function getBaToken()
    {
        return $this->baToken;
    }

    /**
     * @param mixed $baToken
     */
    public function setBaToken($baToken): void
    {
        $this->baToken = $baToken;
    }

    /**
     * @return mixed
     */
    public function getAccountHolderType()
    {
        return $this->accountHolderType;
    }

    /**
     * @param mixed $accountHolderType
     */
    public function setAccountHolderType($accountHolderType): void
    {
        $this->accountHolderType = $accountHolderType;
    }

    /**
     * @return mixed
     */
    public function getAccountHolderName()
    {
        return $this->accountHolderName;
    }

    /**
     * @param mixed $accountHolderName
     */
    public function setAccountHolderName($accountHolderName): void
    {
        $this->accountHolderName = $accountHolderName;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param mixed $accountNumber
     */
    public function setAccountNumber($accountNumber): void
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return mixed
     */
    public function getRoutingNumber()
    {
        return $this->routingNumber;
    }

    /**
     * @param mixed $routingNumber
     */
    public function setRoutingNumber($routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    public function isVerified(): bool
    {
        return $this->valetVerified ?? false;
    }

    public function isFilled(): bool
    {
        return
            $this->routingNumber !== '' &&
            $this->accountNumber !== '' &&
            $this->accountHolderName !== '';
    }

    public function isApproved(): bool
    {
        return
            $this->baToken !== '' &&
            $this->cusToken !== '';
    }

    public function verifyValet()
    {
        $this->valetVerified = true;
    }
}

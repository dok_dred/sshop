<?php


namespace App\Providers;


use App\Http\View\Composers\CartComposer;
use App\Http\View\Composers\MenuComposer;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['furns.components.header', 'furns.components.footer'],
            MenuComposer::class
        );

        View::composer(
            ['public.components.header.cart.list', 'public.components.header.cart.icon', 'public.checkout.cart', 'furns.components.header', 'furns.pages.cart.*'],
            CartComposer::class
        );
    }
}


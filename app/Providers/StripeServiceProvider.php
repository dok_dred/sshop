<?php


namespace App\Providers;


use App\Http\View\Composers\CartComposer;
use App\Http\View\Composers\MenuComposer;
use Illuminate\Support\Facades\View;
use Stripe\Stripe;

class StripeServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }
}


<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function extractTitle(string $title = ''): string
    {
        $appName = config('app.name', 'Laravel');
        if ($title !== '') {
            $appName = sprintf('%s - %s', $appName, $title);
        }
        return $appName;
    }

    public function doResponse(string $view, array $data = []): Response
    {
        if (isset($data['title'])) {
            $data['page_name'] = $data['title'];
        } else {
            $data['page_name'] = $data['title'] = '';
        }

        $data['title'] = static::extractTitle($data['title']);

        return response()->view($view, $data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public function index()
    {
        $title = "Список пунктов меню";
        $menuList = [];
        foreach (Menu::all() as $item) {
            $menuList[$item->id] = $item;
        }

        return $this->doResponse('admin.menu.list', compact('menuList', 'title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'name' => 'required',
                'path' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.menu.create')->withErrors($validator)->withInput();
        }

        $menu = new Menu();

        $menu->path = $request->get('path');
        $menu->name = $request->get('name');
        $menu->parent_id = $request->get('parent_id') ?? 0;
        $menu->sort = $request->get('sort') ?? 0;

        $menu->saveOrFail();

        return response()->redirectToRoute('admin.menu.list')->with('success', 'Пункт меню успешно добавлен');
    }

    public function edit(Menu $menu)
    {
        $title = "Редактирование пункта меню";
        $menuList = Menu::all()->pluck('name', 'id')->toArray();
        return $this->doResponse('admin.menu.edit', compact('menu', 'title', 'menuList'));
    }

    public function create()
    {
        $title = "Создание пункта меню";
        $menuList = Menu::all()->pluck('name', 'id')->toArray();
        return $this->doResponse('admin.menu.create', compact('title', 'menuList'));
    }

    public function update(Request $request, Menu $menu)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'name' => 'required',
                'path' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.menu.edit', $menu->id)->withErrors(
                $validator
            )->withInput();
        }

        $menu->path = $request->get('path');
        $menu->name = $request->get('name');
        $menu->parent_id = $request->get('parent_id') ?? 0;
        $menu->sort = $request->get('sort') ?? 0;

        $menu->saveOrFail();

        return response()->redirectToRoute('admin.menu.list')->with(
            'success',
            'Пункт меню успешно отредактирован'
        );
    }

    public function destroy(Menu $menu)
    {
        $menu->delete();

        return response()->redirectToRoute('admin.menu.list')->with('success', 'Категория успешно удалена');
    }
}

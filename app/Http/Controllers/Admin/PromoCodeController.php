<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\PromoCode;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Throwable;

class PromoCodeController extends Controller
{

    public function index(): Response
    {
        $title = 'Список промокодов';
        $promoCodes = PromoCode::all();

        return $this->doResponse('admin.promocodes.list', compact('promoCodes', 'title'));
    }


    public function create(): Response
    {
        $title = 'Создание промокода';
        return $this->doResponse('admin.promocodes.create', compact('title'));
    }


    /**
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'code' => 'required',
                'discount' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.promocodes.create')->withErrors($validator)->withInput();
        }

        $promoCode = new PromoCode();

        $this->setPromoCodeFromRequest($promoCode, $request);

        $promoCode->saveOrFail();

        return response()->redirectToRoute('admin.promocodes.list')->with('success', 'промокод успешно добавлен');
    }

    public function edit(PromoCode $promoCode): Response
    {
        $title = 'Изменение промокода';
        return $this->doResponse('admin.promocodes.edit', compact('promoCode', 'title'));
    }


    /**
     * @throws Throwable
     */
    public function update(Request $request, PromoCode $promoCode)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'code' => 'required',
                'discount' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.promocodes.create')->withErrors($validator)->withInput();
        }

        $this->setPromoCodeFromRequest($promoCode, $request);

        $promoCode->saveOrFail();

        return response()->redirectToRoute('admin.promocodes.list')->with('success', 'промокод успешно обновлен');
    }


    public function destroy(PromoCode $promoCode): RedirectResponse
    {
        $promoCode->delete();

        return response()->redirectToRoute('admin.promocodes.list')->with('success', 'промокод успешно удален');
    }

    private function setPromoCodeFromRequest(PromoCode $promoCode, Request $request)
    {
        $promoCode->code = $request->get('code');
        $promoCode->discount = $request->get('discount');
    }
}

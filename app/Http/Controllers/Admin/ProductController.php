<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Product;
use App\Service\UploaderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    private $uploader;

    public function __construct(UploaderService $uploader)
    {
        $this->uploader = $uploader;
    }

    public function index(): \Illuminate\Http\Response
    {
        $title = 'Список продуктов';
        $products = Product::all();

        return $this->doResponse('admin.products.list', compact('products', 'title'));
    }


    public function create(): \Illuminate\Http\Response
    {
        $title = 'Создание продукта';
        $categories = Category::all()->pluck('category', 'id')->toArray();
        return $this->doResponse('admin.products.create', compact('title', 'categories'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'title' => 'required',
                'price' => 'required|numeric',
                'category' => 'required',
                'photo' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.products.create')->withErrors($validator)->withInput();
        }

        $product = new Product();

        $this->setProductFromRequest($product, $request);

        $category = Category::query()->findOrFail($request->get('category'));

        $product->category()->associate($category);

        $product->saveOrFail();

        return response()->redirectToRoute('admin.products.list')->with('success', 'Продукт успешно добавлен');
    }

    public function edit(Product $product): \Illuminate\Http\Response
    {
        $title = 'Изменение продукта';
        $categories = Category::all()->pluck('category', 'id')->toArray();
        return $this->doResponse('admin.products.edit', compact('product', 'title', 'categories'));
    }


    public function update(Request $request, Product $product)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'title' => 'required',
                'price' => 'required',
                'category' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.products.create')->withErrors($validator)->withInput();
        }

        $this->setProductFromRequest($product, $request);


        if ($product->category->id !== $request->get('category')) {
            $category = Category::query()->findOrFail($request->get('category'));
            $product->category()->associate($category);
        }

        $product->saveOrFail();

        return response()->redirectToRoute('admin.products.list')->with('success', 'Продукт успешно обновлен');
    }


    public function destroy(Product $product): \Illuminate\Http\RedirectResponse
    {
        $product->delete();

        return response()->redirectToRoute('admin.products.list')->with('success', 'Продукт успешно удален');
    }

    public function deleteComment(Product $product, $commentId): \Illuminate\Http\RedirectResponse
    {
        $product->deleteComment($commentId);
        $product->save();
        return back();
    }

    private function setProductFromRequest(Product &$product, Request $request)
    {
        $product->title = $request->get('title');
        $product->price = $request->get('price');
        $product->discount = $request->get('discount');
        $product->description = $request->get('description');

        $photo = [];

        $files = $request->file('photo');
        if (count($files) > 0) {
            foreach ($request->file('photo') as $file) {
                $photo[] = $this->uploader->upload($file)['url'];
            }
            $product->photo = $photo;
        }
    }
}

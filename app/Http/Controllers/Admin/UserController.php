<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $title = 'Список пользователей';
        return $this->doResponse('admin.users.index', compact('users', 'title'));
    }

    public function setAdmin(User $user)
    {
        $user->setAdmin();
        $user->save();

        return response()->redirectToRoute('admin.users.index')->with('success', 'Статус пользователя успешно изменен');
    }

    public function unsetAdmin(User $user)
    {
        $user->unsetAdmin();
        $user->save();

        return response()->redirectToRoute('admin.users.index')->with('success', 'Статус пользователя успешно изменен');
    }

    public function delete(User $user)
    {
        $user->delete();

        return response()->redirectToRoute('admin.users.index')->with('success', 'Gользователm успешно удален');
    }


}

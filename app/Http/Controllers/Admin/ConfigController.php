<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{
    public function index()
    {
        $title = "Список конфигураций";
        $cfgList = Config::all();

        return $this->doResponse('admin.config.list', compact('cfgList', 'title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'name' => 'required',
                'key' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.config.create')->withErrors($validator)->withInput();
        }

        $cfg = new Config();

        $cfg->name = $request->get('name');
        $cfg->key = $request->get('key');
        $cfg->value = $request->get('value');

        $cfg->saveOrFail();

        return response()->redirectToRoute('admin.config.list')->with('success', 'Конфигурация успешно добавлена');
    }

    public function edit(Config $cfg)
    {
        $title = "Редактирование конфигурации";
        $cfgList = Config::all();
        return $this->doResponse('admin.config.edit', compact('cfg', 'title', 'cfgList'));
    }

    public function create()
    {
        $title = "Создание конфигурации";
        return $this->doResponse('admin.config.create', compact('title'));
    }

    public function update(Request $request, Config $cfg)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'name' => 'required',
                'key' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.config.edit', $cfg->id)->withErrors(
                $validator
            )->withInput();
        }

        $cfg->name = $request->get('name');
        $cfg->key = $request->get('key');
        $cfg->value = $request->get('value');

        $cfg->saveOrFail();

        return response()->redirectToRoute('admin.config.list')->with(
            'success',
            'Конфигурация успешно отредактирована'
        );
    }

    public function destroy(Config $cfg)
    {
        $cfg->delete();

        return response()->redirectToRoute('admin.config.list')->with('success', 'Конфигурация успешно удалена');
    }
}

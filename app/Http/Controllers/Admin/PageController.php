<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Page;
use Illuminate\Http\Request;
use Validator;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        $title = 'Список страниц';
        return $this->doResponse('admin.pages.list', compact('pages', 'title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'path' => 'required|unique:pages'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.pages.create')->withErrors($validator)->withInput();
        }

        $page = new Page();

        $page->path = $request->get('path');
        $page->title = $request->get('title');
        $page->content = $request->get('content');

        try {
            $page->saveOrFail();
        } catch (\Throwable $e) {
            return response()->redirectToRoute('admin.pages.create')->with('error', $e->getMessage())->withInput();
        }

        return response()->redirectToRoute('admin.pages.list')->with('success', 'Страница успешно добавлена');
    }

    public function edit(Page $page)
    {
        $title = 'Редактирование страницы';
        return $this->doResponse('admin.pages.edit', compact('page', 'title'));
    }

    public function create()
    {
        $title = 'Создание страницы';
        return $this->doResponse('admin.pages.create', compact('title'));
    }

    public function update(Request $request, Page $page)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'path' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.pages.edit', $page->id)->withErrors($validator);
        }

        $page->path = $request->get('path');
        $page->title = $request->get('title');
        $page->content = $request->get('content');

        $page->saveOrFail();

        return response()->redirectToRoute('admin.pages.list')->with('success', 'Страница успешно отредактирована');
    }

    public function destroy(Page $page)
    {
        $page->delete();

        return response()->redirectToRoute('admin.pages.list')->with('success', 'Страница успешно удалена');
    }
}

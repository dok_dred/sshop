<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $title = "Список категорий";
        $categories = Category::all();

        return $this->doResponse('admin.categories.list', compact('categories', 'title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'category' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.categories.create')->withErrors($validator)->withInput();
        }

        $category = new Category();

        $category->category = $request->get('category');

        $category->saveOrFail();

        return response()->redirectToRoute('admin.categories.list')->with('success', 'Категория успешно добавлена');
    }

    public function edit(Category $category)
    {
        $title = "Редактирование категории";
        return $this->doResponse('admin.categories.edit', compact('category', 'title'));
    }

    public function create()
    {
        $title = "Создание категории";
        return $this->doResponse('admin.categories.create', compact('title'));
    }

    public function update(Request $request, Category $category)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'category' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->redirectToRoute('admin.categories.edit', $category->id)->withErrors(
                $validator
            )->withInput();
        }

        $category->category = $request->get('category');

        $category->saveOrFail();

        return response()->redirectToRoute('admin.categories.list')->with(
            'success',
            'Категория успешно отредактирована'
        );
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return response()->redirectToRoute('admin.categories.list')->with('success', 'Категория успешно удалена');
    }
}

<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Service\UploaderService;
use Illuminate\Http\Request;

class MainController extends Controller
{
    private $uploader;

    public function __construct(UploaderService $uploader)
    {
        $this->uploader = $uploader;
    }

    public function index()
    {
        $title = 'Административная панель';
        return $this->doResponse('admin.index', compact('title'));
    }

    public function upload(Request $request)
    {
        $response = $this->uploader->upload($request->file('upload'));
        return response()->json($response);
    }
}

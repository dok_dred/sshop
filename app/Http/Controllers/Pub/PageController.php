<?php

namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Config;
use App\Model\Page;
use App\Model\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function show(Page $page): Response
    {
        $title = $page->title;
        return $this->doResponse('furns.pages.page', compact('title','page'));
    }

    public function home(): Response
    {
        $products = [];
        $slider = Product::orderByDesc('id')->limit(2)->get();
        $title = Config::getValue('home_title');
        $categoriesObj = Category::all();

        $nullCategory = new Category();
        $nullCategory->id = 0;
        $nullCategory->category = "All";

        $categoriesObj->add($nullCategory);
        $categories = $categoriesObj->sortBy('id');


        foreach (Product::all() as $product) {
            $products[0][] = $product;
            $products[$product->category->id][] = $product;
        }

        $page = Page::query()->where('path', '=', 'home')->first();
        return $this->doResponse('furns.pages.home', compact('title', 'slider', 'categories', 'page', 'products'));
    }
}

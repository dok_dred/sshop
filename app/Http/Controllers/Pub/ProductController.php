<?php

namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Config;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $count = (int)Config::getValue('page_count');
        $categoryID = (int)$request->get('category') ?? 0;
        if ($categoryID !== 0 && $categoryID !== '0') {
            $products = Product::whereCategoryId($categoryID)->paginate($count);
        } else {
            $products = Product::paginate($count);
        }

        $title = Config::getValue('shop_title');
        $pagination = $products->toArray();
        $categories = Category::all();

        $nullCategory = new Category();
        $nullCategory->id = 0;
        $nullCategory->category = "All";

        $categories->add($nullCategory);
        $categories = $categories->sortBy('id');
        $allCount = Product::all()->count();

        return $this->doResponse('furns.pages.shop.list', compact('products', 'title', 'pagination', 'categories', 'allCount'));
    }

    public function show(Product $product)
    {
        $sameProducts = Product::query()
            ->where('id', '!=', $product->id)
            ->where('category_id', '=', $product->category_id)
            ->get();
        $featureProducts = Product::query()
            ->where('id', '!=', $product->id)
            ->get();

        return $this->doResponse('furns.pages.shop.detail', compact('product', 'featureProducts', 'sameProducts'));
    }

    public function addComment(Request $request, Product $product)
    {
        $validator = Validator::make(
            $request->toArray(),
            [
                'name' => 'required',
                'comment' => 'required',
            ]
        );
        if ($validator->fails()) {
            return back()->withErrors(
                $validator
            )->withInput();
        }
        $comment = [
            'id' => uniqid('comment'),
            'name' => $request->get('name'),
            'comment' => $request->get('comment'),
        ];
        $product->addComment($comment);
        $product->save();

        return back();
    }
}

<?php

namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;
use App\Model\Config;
use App\Model\Valet;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;

class AccountController extends Controller
{

    public function account(): Response
    {
        $stripePk = env('STRIPE_PUBLIC_KEY');
        $title = Config::getValue('checkout_title');
        return $this->doResponse('furns.pages.account.index', compact('stripePk', 'title'));
    }

    /**
     * @throws ApiErrorException
     */
    public function approve(Request $request): RedirectResponse
    {
        $user = Auth::user();
        $valet = $user->getValet();

        $bankAccount = $request->get('bank_account');

        $customer = Customer::create(
            [
                'description' => 'Shop customer',
                'source' => $request->get('token'),
            ]
        );

        $valet->setBaToken($bankAccount);
        $valet->setCusToken($customer->id);
        $user->setValet($valet);
        $user->save();

        return back()->with('success', 'Bank data approved');
    }

    public function add(Request $request): RedirectResponse
    {
        $valet = new Valet();

        $valet->setAccountHolderName($request->get('account_holder_name'));
        $valet->setAccountNumber($request->get('account_number'));
        $valet->setRoutingNumber($request->get('routing_number'));

        Auth::user()->setValet($valet);

        Auth::user()->save();

        return back()->with('success', 'Bank data added');
    }

    public function clear(): RedirectResponse
    {
        $user = Auth::user();
        $user->clearValet();
        $user->save();

        return back()->with('success', 'Bank data cleared');
    }

    public function verify(Request $request): RedirectResponse
    {
        $user = Auth::user();
        $valet = $user->getValet();

        $first = $request->get('first');
        $second = $request->get('second');

        try {
            $bankAccount = Customer::retrieveSource(
                $valet->getCusToken(),
                $valet->getBaToken()
            );
        } catch (ApiErrorException $e) {
            return back()->with('error', $e->getMessage());
        }

        try {
            $bankAccount->verify(['amounts' => [(int)$first, (int)$second]]);
        } catch (ApiErrorException $e) {
            return back()->with('error', $e->getMessage());
        }

        if ($bankAccount->status === 'verified') {
            $valet->verifyValet();
            $user->setValet($valet);
            $user->save();
            return back()->with('success', 'Bank data verified');
        }

        return back()->with('error', 'Bank data is not verified. Clear all bank data and make it again');
    }
}

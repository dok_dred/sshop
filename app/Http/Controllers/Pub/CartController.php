<?php


namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;
use App\Model\Config;
use App\Model\Product;
use App\Model\PromoCode;
use Cookie;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Overtrue\LaravelShoppingCart\Facade as Cart;
use Overtrue\LaravelShoppingCart\Item;
use Stripe\Charge;
use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;

class CartController extends Controller
{
    public function add(Request $request, Product $product): RedirectResponse
    {
        $cartProduct = Cart::get($product->id);
        if ($cartProduct === null) {
            Cart::add(
                $product->id,
                $product->title,
                $request->get('quantity') ?? 1,
                $product->priceWithAllDiscount(),
            );
        } else {
            Cart::update(
                $product->id,
                $request->get('quantity') ?? 1
            );
        }

        return back();
    }

    public function removeItem(Request $request, string $itemId): RedirectResponse
    {
        $cartProduct = Cart::get($itemId);
        if (($cartProduct->qty === 0 || $request->get('quantity') === 0) && $cartProduct !== null) {
            $this->delete($itemId);
        }

        Cart::update(
            $itemId,
            $cartProduct->qty - 1
        );

        return back();
    }

    public function delete(string $itemId): RedirectResponse
    {
        Cart::remove($itemId);
        return back();
    }

    public static function getAllCartProducts(): array
    {
        return self::getPreparedCart();
    }

    private static function getPreparedCart(): array
    {
        $items = Cart::all()->all();
        $qty = Cart::count();
        $total = 0;
        $totalWithoutDiscount = 0;

        $productIds = [];
        $cartItems = [];
        /** @var Item $item */
        foreach ($items as &$item) {
            $item = $item->all();
            $cartItems[$item['id']] = $item;
            $productIds[] = $item['id'];
        }

        $products = [];
        foreach (Product::find($productIds) as $product) {
            $products[$product->id] = $product;
            $total += $cartItems[$product->id]['qty'] * $product->priceWithAllDiscount();
            $totalWithoutDiscount += $cartItems[$product->id]['qty'] * $product->price;
        }

        return [
            'items' => $items,
            'qty' => $qty,
            'total' => $total,
            'products' => $products,
            'total_without_discount' => $totalWithoutDiscount
        ];
    }

    public function index(): Response
    {
        $title = Config::getValue('cart_title');
        return $this->doResponse('furns.pages.cart.index', compact('title'));
    }

    public function removeAll(): RedirectResponse
    {
        Cart::clean();
        return back();
    }

    /**
     * @throws ApiErrorException
     */
    public function checkout(): Response
    {
        $url = env('APP_URL');
        $lineItems = [];
        $sessionId = '';
        $cart = self::getPreparedCart();

        foreach ($cart['items'] as $cartItem) {
            $product = Product::find($cartItem['id']);
            $lineItems[] = [
                'price_data' => [
                    'currency' => 'usd',
                    'product_data' => [
                        'name' => $cartItem['name'],
                    ],
                    'unit_amount' => $product->priceWithAllDiscount() * 100,
                ],
                'quantity' => $cartItem['qty'],
            ];
        }

        if ($lineItems !== []) {
            $session = Session::create(
                [
                    'payment_method_types' => ['card'],
                    'line_items' => $lineItems,
                    'mode' => 'payment',
                    'success_url' => $url . '/payment/success?session_id={CHECKOUT_SESSION_ID}',
                    'cancel_url' => $url . '/payment/cancel',
                ]
            );

            $sessionId = $session->id;
        }
        $title = Config::getValue('cart_title');
        $stripePk = env('STRIPE_PUBLIC_KEY');
        return $this->doResponse('furns.pages.cart.checkout', compact('sessionId', 'stripePk', 'title'));
    }

    public function success(Request $request): Response
    {
        $status = 'Success';
        Cart::clean();
        $response = json_encode($request->all());
        return $this->doResponse('furns.pages.cart.payment', compact('status', 'response'));
    }

    public function cancel(Request $request): Response
    {
        $status = 'Operation cancelled by payment';
        $response = json_encode($request->all());
        return $this->doResponse('furns.pages.cart.payment', compact('status', 'response'));
    }

    public function bankPay(): RedirectResponse
    {
        $cart = self::getPreparedCart();
        $valet = Auth::user()->getValet();
        try {
            $obj = Charge::create(
                [
                    'amount' => $cart['total'] * 100,
                    'currency' => 'usd',
                    'customer' => $valet->getCusToken(),
                ]
            );

            return response()->redirectToRoute('public.payment.success', ['obj' => $obj->toArray()]);
        } catch (Exception $e) {
            return response()->redirectToRoute('public.payment.cancel', ['message' => $e->getMessage()]);
        }
    }

    public function updateCart(Request $request): RedirectResponse
    {
        $items = Cart::all()->all();

        foreach ($items as $item) {
            $item = $item->all();
            if ($qty = $request->get('product-' . $item['id'])) {
                Cart::update(
                    $item['__raw_id'],
                    $qty
                );
            }
        }

        return back();
    }

    public function activateCode(Request $request): RedirectResponse
    {
        $code = PromoCode::where('code', '=', $request->get("code"))->first();
        $response = back();
        if ($code) {
            return $response->withCookie(cookie('discount', $code->discount))->withCookie(cookie('code', $code->code));
        }
        return $response;
    }

    public function deactivateCode(): RedirectResponse
    {
        Cookie::queue(Cookie::forget('discount'));
        Cookie::queue(Cookie::forget('code'));
        return back();
    }
}

<?php


namespace App\Http\View\Composers;


use App\Model\Menu;
use Illuminate\View\View;

class MenuComposer
{
    public function compose(View $view)
    {
        $view->with('menu', Menu::tree());
        $view->with('footerMenu', Menu::query()->orderBy('sort')->get());
    }
}

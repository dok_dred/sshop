<?php


namespace App\Http\View\Composers;


use App\Http\Controllers\Pub\CartController;
use Illuminate\View\View;

class CartComposer
{
    public function compose(View $view)
    {
        $view->with('cart', CartController::getAllCartProducts());
    }
}

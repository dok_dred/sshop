<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditPhotoField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $products = \App\Model\Product::all();
        foreach ($products as &$product) {
            if (!is_array($product->photo)) {
                $product->photo = [$product->photo];
                $product->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $products = DB::table('products')->get();
        foreach ($products as &$product) {
            if (is_array($product->photo)) {
                $product->photo = $product->photo[0];
            }
        }
    }
}

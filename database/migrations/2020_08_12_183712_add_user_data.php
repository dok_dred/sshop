<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->string('routing_number');
                $table->string('account_number');
                $table->string('account_holder_name');
                $table->string('account_holder_type');
                $table->string('ba_token');
                $table->string('cus_token');
                $table->boolean('valet_verified');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropColumn('routing_number');
                $table->dropColumn('account_number');
                $table->dropColumn('account_holder_name');
                $table->dropColumn('account_holder_type');
                $table->dropColumn('ba_token');
                $table->dropColumn('cus_token');
                $table->dropColumn('valet_verified');
            }
        );
    }
}
